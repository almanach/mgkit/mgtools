#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
static int verbose_flag;
int keep_comments;
int warning_flag ;
#include <libxml/tree.h>
extern int error_symb ;
int error_sem ;
#include "hash.h"
#include "symbol.h"

extern FILE *yyin;
extern int yyparse (void);
xmlDocPtr document_xml ;
xmlNodePtr MetaGrammar ;
xmlNodePtr CurrentClass = NULL ;
int class_found = 0 ;

void chk_class (char* name, void* tb) {
  struct tbsymb_el* tb_el = (struct tbsymb_el*) tb ;
  if (tb_el->symbol_type != class_symbol) return ;
  if (!tb_el->defs) {
    fprintf (stderr,
	     "Error: class %s is not defined, but has been used at following line(s):\t",
	     name) ;
    prt_list (stderr, tb_el->uses) ;
    fprintf (stderr, "\n") ;
    error_symb++ ;
  }
}

void prt_class (char* name, void* tb) {
  struct tbsymb_el* tb_el = (struct tbsymb_el*) tb ;
  if (tb_el->symbol_type != class_symbol) return ;
  printf ("class %s:\tdefined at line\t", name) ;
  prt_list (stdout, tb_el->defs ) ;
  if (tb_el->uses) {
    printf ("\n\t\tused at line\t", name) ;
    prt_list (stdout, tb_el->uses ) ;
  }
  printf ("\n") ;
}

void chk_macro (char* name, void* tb) {
  struct tbsymb_el* tb_el = (struct tbsymb_el*) tb ;
  if (tb_el->symbol_type != template_macro_symbol) return ;
  if (!tb_el->defs) {
    fprintf (stderr,
	     "Error: macro %s is not defined, but has been used at following line(s):\t",
	     name) ;
    prt_list (stderr, tb_el->defs) ;
    fprintf (stderr, "\n") ;
    error_symb++ ;
  }
}

void prt_macro (char* name, void* tb) {
  struct tbsymb_el* tb_el = (struct tbsymb_el*) tb ;
  if (tb_el->symbol_type != template_macro_symbol) return ;
  printf ("macro %s:\tdefined at line\t", name) ;
  prt_list (stdout, tb_el->defs ) ;
  if (tb_el->uses) {
    printf ("\n\t\tused at line\t", name) ;
    prt_list (stdout, tb_el->uses ) ;
  }
  printf ("\n") ;
}

void chk_path_macro (char* name, void* tb) {
  struct tbsymb_el* tb_el = (struct tbsymb_el*) tb ;
  if (tb_el->symbol_type != path_macro_symbol) return ;
  if (!tb_el->defs) {
    fprintf (stderr,
	     "Error: path macro %s is not defined, but has been used at following line(s):\t",
	     name) ;
    prt_list (stderr, tb_el->defs) ;
    fprintf (stderr, "\n") ;
    error_symb++ ;
  }
}

void prt_path_macro (char* name, void* tb) {
  struct tbsymb_el* tb_el = (struct tbsymb_el*) tb ;
  if (tb_el->symbol_type != path_macro_symbol) return ;
  printf ("path macro %s:\tdefined at line(s)\t", name) ;
  prt_list (stdout, tb_el->defs ) ;
  if (tb_el->uses) {
    printf ("\n\t\tused at line\t", name) ;
    prt_list (stdout, tb_el->uses ) ;
  }
  printf ("\n") ;
}

void prt_undef (char* name, void* tb) {
  struct tbsymb_el* tb_el = (struct tbsymb_el*) tb ;
  if (tb_el->symbol_type != undef_symbol) return ;
  printf ("undef %s:\t", name) ;
  if (tb_el->defs) {
    printf ("defined: ") ; prt_list (stdout, tb_el->defs ) ;
  }
  if (tb_el->uses) {
    printf ("\n\t\tused at line\t", name) ;
    prt_list (stdout, tb_el->uses ) ;
  }
  printf ("\n") ;
}

main (int argc, char *argv[])
{
  int c, j ;
  extern FILE *yyin ;
  char *yyinfile ;
  warning_flag = 0 ;
  keep_comments = 1 ;
  while (1) {
      static struct option long_options[] =
	{
	  /* These options set a flag. */
	  {"verbose", no_argument,       &verbose_flag, 1},
	  {"brief",   no_argument,       &verbose_flag, 0},
	  /* These options don't set a flag.
	     We distinguish them by their indices. */
	  {"warning",     no_argument,       0, 'w'},
	  {"comments",     no_argument,       0, 'c'},
	  {"no_comments",     no_argument,       0, 'n'},
	  {0, 0, 0, 0}
	};
      int option_index = 0 ;
     
      c = getopt_long (argc, argv, "wcn",
		       long_options, &option_index) ;
      if (c == -1) break ;
      switch (c)
	{
	case 0:
	  if (long_options[option_index].flag != 0)
	    break;
	  if (optarg) printf (" with arg %s", optarg) ;
	  break ;
	case 'w':
	  warning_flag = 1 ;
	  break ;
	case 'c':
	  keep_comments = 1 ;
	  break;
	case 'n':
	  keep_comments = 0;
	  break;
	default:
	  abort ();
	}
      
  }
  /* Print any remaining command line arguments (not options). */
  if (optind < argc)
    {
      yyinfile = argv[optind++] ;
      if (!(yyinfile && (yyin = fopen (yyinfile, "r")))) {
	printf ("smg2xml: Input file not found : %s\n", yyinfile) ;
	exit (1) ;
      }
    }
  else {
    yyin = stdin ;
  } 
  error_symb = 0 ;
  document_xml = xmlNewDoc((xmlChar*)"1.0") ;
  MetaGrammar = xmlNewDocNode(document_xml, NULL, (xmlChar*)"metaGrammar", NULL);
  document_xml->children = MetaGrammar ;
  j = yyparse() ;
  fclose (yyin) ;
  if (j) printf ("yyparse returned %d\n", j) ;
  if (warning_flag) {
    iter_tab (&chk_class) ;
    iter_tab (&chk_macro) ;
    iter_tab (&chk_path_macro) ;
  }
  xmlSaveFormatFileEnc ("-", document_xml,(xmlChar*)"ISO-8859-1",1) ;
  /*   iter_tab (&prt_class) ; */
  /*   iter_tab (&prt_macro) ; */
  /*   iter_tab (&prt_path_macro) ; */
  /*   iter_tab (&prt_undef) ; */
  if (error_symb) fprintf (stderr,"error_symb=%d\n",error_symb) ;
  exit(error_symb) ;
}
