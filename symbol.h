typedef enum {class_symbol, template_macro_symbol, path_macro_symbol, undef_symbol} symb_t ;

struct int_list {
    int val ;
    struct int_list* next ;
} ;
void prt_list (FILE*,struct int_list*) ;
struct tbsymb_el {
  symb_t symbol_type ;
  char * value;
  struct int_list * defs ;
  struct int_list * uses ;
} ;
void add_def (struct tbsymb_el*, int) ;
void add_use (struct tbsymb_el*, int) ;
struct tbsymb_el* new_class_symb (char*, int, int) ;
struct tbsymb_el* new_template_macro_symb (char*, int, int) ;
struct tbsymb_el* new_path_macro_symb  (char*, int, int) ;
