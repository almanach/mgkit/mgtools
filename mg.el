;;; mg.el --- Editing Meta Grammar under Emacs

;; Copyright (C) 2004, 2006, 2011  Free Software Foundation, Inc.

;; Author: De la Clergerie Eric <Eric.De_La_Clergerie@inria.fr>
;; Keywords: languages

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; 

;;; Code:

(require 'comint)
(require 'easymenu)
(require 'imenu)

;;; Constants controlling the mode.
;;  =======================================================================
;;  These are the constants you might want to change

(defvar mg-help-address "Eric.De_La_Clergerie@inria.fr"
  "*Address accepting submission of bug reports.")

(defvar mg-display-program-name "mgviewer"
  "*Program to use to display a Meta Grammar.")

;;;  You probably don't want to change these
;;  =======================================================================

(defcustom mg-process-asynchronous (not (eq system-type 'ms-dos))
  "*Use asynchronous processes."
  :group 'mg-commands
  :type 'boolean)

(defconst mg-mode-version "$Id"
  "mg-mode version number.")


(defconst mg-load-command "open file %s"
  "*Template for loading a file into the MG.")


(defvar mg-mode-abbrev-table nil
  "Abbrev table in use in mg-mode buffers.")
(define-abbrev-table 'mg-mode-abbrev-table ())

(defvar mg-synonym-alist nil)
(defvar mg-synonym-names nil)

;; O for Perl regexps. This is not foolproof.
(defconst mg-prompt-regexp 
"\\(Command:\\|Sim:\\|Which move\\|Hit return to continue:\\|\n.*(y or n)\\)"
  "*Regexp used to recognise prompts in the MG process.")

(defvar mg-temp-file (make-temp-name "/tmp/mg")
  "*Temp file that emacs uses to communicate with the MG process.
 Defaults to (make-temp-name \"/tmp/mg\").")

(defconst mg-output-eaten-buffer "*MG output eaten*")
(defconst mg-temporary-output-buffer "*MG temporary output*")
(defconst mg-help-buffer "*MG help*")

(defvar mg-eating nil
  "a flag to indicate if we are reading output from MG" )

(defvar mg-last-char-seen 1
  "A mark which indicates the beginning of a section from which we may want to extract info from MG editor")

(defvar mg-focus-class ""
  "to keep trace of current focus class")

(defvar mg-class-ring-size 32
  "Size of mg class ring.")
(defvar mg-class-ring-index nil
  "Index of last matched mg class.")
(defvar mg-class-ring (make-ring mg-class-ring-size)
  "Ring of classes previously referred to.")


(defcustom mg-imenu-flag t
  "*Non-nil means add a class menu for all XMLMG files."
  :group 'mg-other
  :type 'boolean)

(defcustom mg-imenu-max-lines 3000
  "*The maximum number of lines of the file for imenu to be enabled.
Relevant only when `mg-imenu-flag' is non-nil."
  :group 'mg-other
  :type 'integer)


(defconst mg-running-XEmacs (string-match "XEmacs" emacs-version))

;;; Derived modes
;; =======================================================================

(define-derived-mode xmlmg-mode nxml-mode
  "MGXML" "Major mode for editing XML Meta Grammars.

For information on running an MG process, see the documentation
for mg-mode.

Customisation: Entry to this mode runs the hooks on xmlmg-mode-hook.

Mode map
========
\\{xmlmg-mode-map}"
  (install-mg-keybindings xmlmg-mode-map)
  (define-key xmlmg-mode-map "\C-c\C-r" 'mg-run-interactive)
  (define-key xmlmg-mode-map "\C-c\C-s" 'switch-to-mg)
  (define-key xmlmg-mode-map "\C-c." 'mg-focus-current)
  (define-key xmlmg-mode-map [?\C-c ?\C-.] 'mg-goto-focus)
  (define-key xmlmg-mode-map "\C-cn" 'mg-focus-next-class)
  (define-key xmlmg-mode-map "\C-cp" 'mg-focus-prev-class)
  (define-key xmlmg-mode-map "\C-c>" 'mg-current-children)
  (define-key xmlmg-mode-map "\C-c<" 'mg-current-parents)
  (define-key xmlmg-mode-map "\C-c;" 'mg-next-selected)
  (define-key xmlmg-mode-map "\C-c:" 'mg-prev-selected)

;;  (define-key cwb-file-mode-map "\C-c\C-r" 'mg-send-region)
;;   (if mg-running-XEmacs 
;;       (define-key xmlmg-mode-map 'button3 'mg-popup-file-mode-menu))
  (easy-menu-add xmlmg-mode-menu xmlmg-mode-map)

  ;; Add class index menu
  (make-variable-buffer-local 'imenu-create-index-function)
  (setq imenu-create-index-function 'imenu-default-create-index-function)
  (setq imenu-prev-index-position-function 'mg-prev-class)
  (setq imenu-extract-index-name-function 'mg-class-name)

  (if (and mg-imenu-flag
           (< (count-lines (point-min) (point-max)) mg-imenu-max-lines))
      (imenu-add-to-menubar "MG Classes"))

)

(define-derived-mode mg-mode comint-mode
  "MG" "Major mode for viewing MetaGrammars.

The following commands are available:
\\{mg-mode-map}

Customisation: Entry to this mode runs the hooks on comint-mode-hook and
mg-mode-hook (in that order).

You can send text to the MGVIEWER process from other buffers containing
MG sources."
  (install-mg-keybindings mg-mode-map)
;; stuff that's only relevant to an interactive mg buffer
  (define-key mg-mode-map "\C-c\C-l" 'mg-load-file)
  (define-key mg-mode-map "\C-i"     'mg-complete-command)
;;  (define-key mg-mode-map "\C-c\C-x" 'mg)
  (easy-menu-add mg-mode-menu mg-mode-map)
  (setq comint-prompt-regexp mg-prompt-regexp)
  (setq mg-synonym-alist nil)
  (setq mg-synonym-names nil)
  (setq comint-input-sentinel 'ignore)
  (setq mg-last-char-seen 1)
  (add-hook 'comint-output-filter-functions 'mg-filter nil t)
  (add-hook 'kill-emacs-hook 'mg-tidy))

;;; Key bindings
;; =======================================================================

;; stuff that's relevant to any mg buffer, interactive or not
(defun install-mg-keybindings (map)
  (define-key map "\C-c\C-b" 'mg-submit-bug-report)
  (define-key map "\C-c\C-v" 'mg-mode-version))

;;; Menus
;; =======================================================================

(defvar mg-shared-menu
  '(("Help and docs"
     ["Help on Emacs MG-mode" describe-mode  t])
    ["Submit bug report"     (mg-submit-bug-report)  t]))

(defvar mg-grapher-menu
  '(("Graphical display"
     ["Start viewer" mg t]
     )))

(defvar mg-mode-menu 
  (append
   '(["Load file..." mg-load-file t])
   mg-grapher-menu
   '(("Help from MG"))
   mg-shared-menu
   )
  "The menu for the interactive MG")

(defvar xmlmg-mode-menu 
  (append
   '(

      ["Switch to MG" switch-to-mg t]
      ["MG Focus" mg-focus-current t]
      ["MG Refresh" mg-refresh t]
      ["XMLMG Focus" mg-goto-focus t]
      ["Next Class" mg-focus-next-class t]
      ["Prev Class" mg-focus-prev-class t]
      ["Parent Classes" mg-current-parents t]
      ["Children Classes" mg-current-children t]
      ["Next Selected" mg-next-selected t]
      ["Prev Selected" mg-prev-selected t]
      )
   mg-shared-menu
   )
  "The menu for editing XML Meta Grammar files"
  )

;; (easy-menu-define mg-mode-menu
;;   mg-mode-map
;;   "Menu used in mg-mode."
;;   (append '("MG") mg-mode-menu))


(easy-menu-define xmlmg-mode-menu  
  xmlmg-mode-map
  "Menu used in xmlmg-mode."
  (append '("XMLMG") xmlmg-mode-menu))


;;; Running MG subprocess
;;; ===========================================================================

(defun mg-run-interactive ()
  "Start MG viewer with current XML MG file."
  (interactive)
  (require 'comint)
  (let ((buffer (mg-process-buffer-name (buffer-file-name)))
	(process nil)
	(file buffer-file-name)
	(class (mg-class-name))
	)
    (if file
	(progn 
	  (save-some-buffers)
	  (mg-process-check file)
	  (setq xmlmg-buffer (current-buffer))
	  (with-output-to-temp-buffer buffer)
	  (set-buffer buffer)
	  (setq mg-init-file-name file)
	  (setq buffer-read-only nil)
	  (make-comint-in-buffer "mg" buffer
				 mg-display-program-name
				 nil
				 file
				 "--verbose")
	  (mg-mode)
;;	  (if class
;;	      (comint-simple-send (concat "select focus " class)))
	  ))))

(defun smg-run-interactive ()
  "Start MG viewer with current SMG file."
  (interactive)
  (require 'comint)
  (let ((buffer (mg-process-buffer-name (buffer-file-name)))
	(process nil)
	(file buffer-file-name)
	(class (smg-class-name))
	)
    (if file
	(progn 
	  (save-some-buffers)
	  (mg-process-check file)
	  (setq smg-buffer (current-buffer))
	  (with-output-to-temp-buffer buffer)
	  (set-buffer buffer)
	  (setq mg-init-file-name file)
	  (setq buffer-read-only nil)
	  (make-comint-in-buffer "mg" buffer
				 mg-display-program-name
				 nil
				 file
				 "--verbose")
	  (mg-mode)
;;	  (if class
;;	      (comint-simple-send (concat "select focus " class)))
	  ))))


(defun mg-process-buffer-name (name)
  "Return name of MG buffer associated with the document NAME."
  (concat "*MG " (abbreviate-file-name (expand-file-name name)) "*"))

(defun mg-process-buffer (name)
  "Return the MG buffer associated with the document NAME."
  (get-buffer (mg-process-buffer-name name)))

(defun mg-process (name)
  "Return MG process associated with the document NAME."
  (and mg-process-asynchronous
       (get-buffer-process (mg-process-buffer name))))

(defun mg-process-check (name)
  "Check if a process for the XMLMG document NAME already exist."
  (let ((process (mg-process name)))
    (and process
	 (eq (process-status process) 'run))))

;;; Things useful in xmlmg-mode
;; =======================================================================

(defun switch-to-mg (eob-p)
  "Switch to the MG process buffer, starting the MG if there isn't one.
With argument, positions cursor at end of buffer."
  (interactive "P")
  (let ((file ((buffer-file-name))))
    (if (mg-process-check file)
	(pop-to-buffer (mg-process-buffer file))
      (error "No current process buffer. See variable mg-buffer."))
    (cond (eob-p
	   (push-mark)
	   (goto-char (point-max))))))


;; Interacting with the MG viewer
;; ======================================================================

(defvar mg-buffer nil "*The current MG process buffer.")

(defvar xmlmg-buffer nil "*The current XMLMG buffer.")

(defun mg-send-region (start end &optional and-go)
  "Send the current region to the MG process."

  (interactive "r\nP")
  (setq mg-eating t)
  (comint-send-region (mg-proc) start end)
  (comint-send-string (mg-proc) "\n")
  )

(defun mg-wait-while-eating ()
  (while mg-eating (sleep-for 1))
  )

(defun mg-filter (string)
  (if (not (= (point-max) mg-last-char-seen)) ;; really something new
      ;; we're interested in the whole of the first newly-completed line
      (let ((pmark (progn (goto-char mg-last-char-seen)
			  (beginning-of-line);assume regexp just one line
			  (point-marker))))
	(setq mg-last-char-seen (point-max))
;; Hilight classes
	(mg-do-from-mark
	 pmark ":\\(.+\\)$"
	 '(put-text-property (match-beginning 1) (match-end 1) 'face font-lock-keyword-face)) 
;; Deal with selections:
	(mg-do-from-mark 
	 pmark "^focus[ ]+on[ ]+\\([^ \t\n]+\\)"
	 '(progn 
	    (setq mg-focus-class (match-string 1))
	    (save-excursion
	      (set-buffer xmlmg-buffer)
	      (mg-goto-focus)
	      )
	    )
	 )
	(setq mg-eating nil))))

(defun mg-value-under-point ()
  (interactive)
  "Return the value under or left to the point."
  (save-excursion
    (let (start)
      (skip-chars-backward "^\"")
      (setq start (point))
      (skip-chars-forward "^\"")
      (buffer-substring-no-properties start (point))
      )))

(defun mg-prev-class ()
  (interactive)
  "Move to beginning of current MG class"
  (mg-prev-element "class")
  )

(defun mg-focus-prev-class ()
  (interactive)
  "Go and Focus previous MG class"
  (mg-prev-element "class")
  (mg-focus)
  )

(defun mg-next-class ()
  (interactive)
  "Move to beginning of next MG class"
  (mg-next-element "class")
  )

(defun mg-focus-next-class ()
  (interactive)
  "Go and Focus next MG class"
  (mg-next-element "class")
  (mg-focus)
  )


(defun mg-next-element (name)
  (interactive (list (read-string "class name: " "class")))
  "Move to next xml element of givent type [default: class]"
  (skip-chars-backward "^<>")
  (re-search-forward (concat "<" name "[ \t\n>]"))
  (skip-chars-backward "^<")
  (point)
)

(defun mg-prev-element (name)
  (interactive (list (read-string "class name: " "class")))
  "Move to prev xml element of givent type [default: class]"
  (skip-chars-forward "^<>")
  (re-search-backward (concat "<" name "[ \t\n>/]"))
  ;; (skip-chars-backward "^<")
  (point)
)

(defun mg-attr-value (attr)
  (save-excursion
    (let ((start (point)))
      (skip-chars-forward "^>")		;; move to closing >
      (re-search-backward (concat attr "[ \t]*=[ \t]*\"\\([^\"]*\\)\""))
      (match-string 1)))
  )

(defun mg-class-name ()
  (interactive)
  (save-excursion
    (progn
      (mg-prev-class)
      (mg-attr-value "name")
      )))

(defun mg-say (string)
  (let ((file (buffer-file-name)))
    (if (mg-process-check file)
	(process-send-string (mg-process-buffer file)
			     (concat string "\n")))))

(defun mg-focus-current ()
  (interactive)
  (mg-focus (mg-class-name))
  )

(defun mg-focus (&optional class)
  (interactive (list (read-string "class name: " (mg-class-name))))
  (let ((class (or class (mg-class-name))))
    (mg-say (concat "select focus " class)))
  )

(defun mg-next-selected ()
  (interactive)
  (mg-say "focus +"))

(defun mg-prev-selected ()
  (interactive)
  (mg-say "focus +"))

(defun mg-current-parents ()
  (interactive)
  (mg-parents))

(defun mg-parents (&optional class)
  (interactive (list (read-string "class name: " (mg-class-name))))
  (let ((class (or class (mg-class-name))))
    (mg-say (concat "select supers " class))
    (mg-say (concat "focus ."))
    )
  )

(defun mg-current-children ()
  (interactive)
  (mg-children))

(defun mg-children (&optional class)
  (interactive (list (read-string "class name: " (mg-class-name))))
  (let ((class (or class (mg-class-name))))
    (mg-say (concat "select children " class))
    (mg-say (concat "focus ."))
    )
  )

(defun mg-providing (res)
  (interactive (list (read-string "resource name: " (mg-potential-resource))))
  (mg-say (concat "select providing " res)))

(defun mg-needing (res)
  (interactive (list (read-string "resource name: " (mg-potential-resource))))
  (mg-say (concat "select needing " res)))
      
(defun mg-potential-resource ()
  "Find a potential resource name around current point"
  (save-excursion
    (skip-chars-backward "^<")
    (let ((start (point)))
      (cond
       ((search-forward "provides" (+ start 10) t)
	(goto-char start)
	(mg-attr-value "name"))
       ((search-forward "needs" (+ start 10) t)
	(goto-char start)
	(mg-attr-value "name"))
       ))))

(defun mg-goto-focus ()
  (interactive)
  "Goto to focus class in XML MG file"
  (mg-goto-class mg-focus-class)
  )

(defun mg-potential-class ()
  "Find a potential class name around current point"
  (save-excursion
    (skip-chars-backward "^<")
    (let ((start (point)))
      (cond
       ((search-forward "super" (+ start 7) t)
	(goto-char start)
	(mg-attr-value "name"))
       ((search-forward "class" (+ start 7) t)
	(goto-char start)
	(mg-attr-value "name"))
       (t
	(mg-focus-class)))
      )
    )
  )

(defun mg-goto-class (&optional class)
  (interactive (list (read-string "class name: " (mg-potential-class))))
  "Goto to class in XML MG file"
  (let ((where (point))
	(class (or class mg-focus-class)))
    (save-excursion
      (progn
	(goto-char (point-min))
	(mg-next-class)
	(while (not (string-equal (mg-class-name) class))
;;	  (message "class %s" (mg-class-name))
	  (mg-next-class)
	  )
	(if (string-equal (mg-class-name) class)
	    (setq where (point)))
	)
      )
    (goto-char where)
    (unless (eq class mg-focus-class) (mg-focus class))
    )
  )

(defun mg-refresh ()
  (interactive)
  (mg-say (concat "open " mg-init-file-name))
)


;; Command completion
;; ======================================================================
(defun mg-complete-command (stub)
  (interactive (list (buffer-substring (point) 
				       (save-excursion (forward-word -1)
						       (comint-skip-prompt)
						       (point)))))
  (comint-dynamic-simple-complete 
   stub 
   '( "select"
      "focus"
      "children"
      "children*"
      "supers"
      "supers*"
      "content"
      "relations"
      "relations*"
      "needs"
      "needs*"
      "provides"
      "provides*"
      "needing"
      "providing"
      "delete"
      "class"
      )))


;;; Miscellaneous utility functions
;; =======================================================================

(defun mg-args-to-list (string)
  (let ((where (string-match "[ \t]" string)))
    (cond ((null where) (list string))
	  ((not (= where 0))
	   (cons (substring string 0 where)
		 (mg-args-to-list (substring string (+ 1 where)
					     (length string)))))
	  (t (let ((pos (string-match "[^ \t]" string)))
	       (if (null pos)
		   nil
		   (mg-args-to-list (substring string pos
					       (length string)))))))))

(defun mg-tidy ()
  "Something to add to `kill-emacs-hook' to tidy up tmp files on exit."
  (if (file-readable-p mg-temp-file)
      (delete-file mg-temp-file)))

;; From pmark to the end of the buffer, call fn for each match of regexp
(defun mg-do-from-mark (pmark regexp fn)
  (goto-char pmark)
  (while (search-forward-regexp regexp nil t) (eval fn)))

;; Submitting a bug report
;; ======================================================================
;; Thanks to c++-mode.el by Barry Warsaw for the original of this code:

(defun mg-mode-version ()
  "Echo the current version of mg-mode."
  (interactive)
  (message "Using mg-mode.el %s" mg-mode-version))

(defun mg-submit-bug-report ()
  "Submit via mail a bug report on MG."
  (interactive)
  (require 'reporter)
  (and
   (y-or-n-p "Do you want to submit a report on either MG? ")
   (reporter-submit-bug-report
    mg-help-address
    (concat "mg-mode.el " mg-mode-version " ")
    nil 
    nil
    "Thank you for submitting a bug report. \nPlease include as much information as possible about the bug."

    )))


;; ======================================================================
;; New simplified mode for editing MG

(defvar smg-mode-abbrev-table nil
  "Abbrev table in use in smg-mode buffers.")
(define-abbrev-table 'smg-mode-abbrev-table ())

(defvar smg-focus-class ""
  "to keep trace of current focus class")

(defcustom smg-imenu-flag t
  "*Non-nil means add a class menu for all XMLMG files."
  :group 'smg-other
  :type 'boolean)

(defcustom smg-imenu-max-lines 3000
  "*The maximum number of lines of the file for imenu to be enabled.
Relevant only when `smg-imenu-flag' is non-nil."
  :group 'smg-other
  :type 'integer)



(defcustom smg-keyword-face 'font-lock-keyword-face
  "SMG classes."
  :type 'face
  :group 'smg
  )

(defcustom smg-op-face 'font-lock-builtin-face
  "SMG operators."
  :type 'face
  :group 'smg
  )

(defvar smg-feature-face 'smg-feature-face)
(defface smg-feature-face
  '((((class color)) (:foreground "orange")))
  "SMG features."
  :group 'smg)

(defvar smg-resource-face 'smg-resource-face)
(defface smg-resource-face
  '((((class color)) (:foreground "red")))
  "SMG resources."
  :group 'smg)

(defcustom smg-class-face 'font-lock-function-name-face
  "SMG classes."
  :type 'face
  :group 'smg
  )

(defcustom smg-variable-face 'font-lock-variable-name-face
  "SMG variables."
  :type 'face
  :group 'smg
  )

(defcustom smg-namespace-face 'font-lock-constant-face
  "SMG namespaces."
  :type 'face
  :group 'smg
  )

(defvar smg-node-face 'smg-node-face)
(defface smg-node-face
  '((((class color)) (:foreground "green")))
  "SMG nodes."
  :group 'smg)

(defcustom smg-macro-face 'font-lock-reference-face
  "SMG macro."
  :type 'macro
  :group 'smg
  )


(defconst smg-namespace "\\w+")

(defconst smg-identifier 
  (concat "\\(?:" smg-namespace "::[ \t]*\\)*\\(\\w+\\)")
  )

(defconst smg-font-lock-keywords
  (list
   (list (regexp-opt '("|" "=" "==" "~" "^") t) 1 smg-op-face)
   (list (concat "[ \t]\\([+-]\\)[ \t]*" smg-identifier)
	 '(1 font-lock-builtin-face)
	 '(2 smg-resource-face)
	 )
   (list "^[ \t]*\\(class\\|disable\\)\\>[ \t]*\\(\\w+\\)?" 
	 '(1 font-lock-keyword-face)
	 '(2 smg-class-face)
	 )
   (list "^[ \t]*\\(template\\|path\\)\\>[ \t]*\\(@\\w+\\)?" 
	 '(1 font-lock-keyword-face)
	 '(2 smg-macro-face)
	 )
   (list "\\(<:\\)[ \t]*\\(\\w+\\)"
	 '(1 font-lock-builtin-face)
	 '(2 smg-class-face)
	 )
   (list (regexp-opt '("opt" "desc" "value" "domain" "property") 'words) 
	 1 smg-keyword-face)
   (list (concat (regexp-opt '("node") 'words)
		 " *" smg-identifier)
	 '(1 smg-keyword-face)
	 '(2 smg-node-face)
	 )
   (list "\\(\\w+\\)[ \t]*:[^:]" 1 smg-feature-face)
   (list "\\. *\\(\\w+\\)" 1 smg-feature-face)
   (list "\\. *\\(@\\w+\\)" 
	 '(1 smg-macro-face))

   (list (concat (regexp-opt '("node" "father") 'words)
		 "( *" smg-identifier " *)")
	 '(1 smg-keyword-face)
	 '(2 smg-node-face)
	 )

   (list (concat  smg-identifier "[ \t]*"
		  (regexp-opt '(">>" ">>+" "<" "=") t)
		  "[ \t]*" smg-identifier
		  )
	 '(1 smg-node-face)
	 '(2 smg-op-face)
	 '(3 smg-node-face)
	 )
   
   (list (concat smg-identifier "[ \t]*\\(=>\\|+\\)")
	 '(1 smg-node-face)
	 '(2 smg-keyword-face))

   (list (concat (regexp-opt '("$") 't)	smg-identifier)
	 '(1 smg-keyword-face)
	 '(2 smg-variable-face)
	 )

   (list (concat "\\(" smg-namespace "\\)\\(::\\)")
	 '(1 smg-namespace-face)
	 '(2 smg-op-face))
   
   )
  "Default hilight for sgm mode"
)

(define-derived-mode smg-mode c-mode
  "SMG" "Major mode for editing Meta Grammars.

For information on running an MG process, see the documentation
for smg-mode.

Customisation: Entry to this mode runs the hooks on xmlmg-mode-hook.

Mode map
========
\\{smg-mode-map}"
  (install-mg-keybindings smg-mode-map)
  (define-key smg-mode-map "\C-c\C-r" 'smg-run-interactive)
  (define-key smg-mode-map "\C-c\C-s" 'switch-to-mg)
  (define-key smg-mode-map "\C-c." 'smg-focus-current)
  (define-key smg-mode-map [?\C-c ?\C-.] 'smg-goto-focus)
  (define-key smg-mode-map "\C-cn" 'smg-focus-next-class)
  (define-key smg-mode-map "\C-cp" 'smg-focus-prev-class)
  (define-key smg-mode-map "\C-c>" 'smg-children)
  (define-key smg-mode-map "\C-c<" 'smg-parents)
  (define-key smg-mode-map "\C-c;" 'smg-next-selected)
  (define-key smg-mode-map "\C-c:" 'smg-prev-selected)

;;  (define-key cwb-file-mode-map "\C-c\C-r" 'mg-send-region)
;;   (if mg-running-XEmacs 
;;       (define-key smg-mode-map 'button3 'mg-popup-file-mode-menu))
  (easy-menu-add smg-mode-menu smg-mode-map)

  ;; Add class index menu
  (make-variable-buffer-local 'imenu-create-index-function)
  (setq imenu-create-index-function 'imenu-default-create-index-function)

  ;; WARNING: The following lines may block loading SMG files
  ;; DON'T KNOW WHY !
;;  (setq imenu-prev-index-position-function 'smg-prev-class)
;;  (setq imenu-extract-index-name-function 'smg-class-name)

  (if (and smg-imenu-flag
           (< (count-lines (point-min) (point-max)) smg-imenu-max-lines))
      (imenu-add-to-menubar "MG Classes"))

  (set (make-local-variable 'comment-start) "%")
  (set (make-local-variable 'comment-end) "")
  (set (make-local-variable 'comment-start-skip) "%+\\s*")

  (modify-syntax-entry ?- "w")
  (modify-syntax-entry ?% "< b")

  (set (make-local-variable 'font-lock-defaults) '(smg-font-lock-keywords nil nil ((?_ . "w"))))

  ;; (set (make-local-variable 'font-lock-support-mode) 'lazy-lock-mode)
  ;; (set (make-local-variable 'lazy-lock-defer-contextually) nil)
  ;; (set (make-local-variable 'lazy-lock-defer-on-the-fly) t)
  (set (make-local-variable 'lazy-lock-defer-time) 0.1)
  ;; (set (make-local-variable 'lazy-lock-defer-on-scrolling) t)

  (turn-on-font-lock)

  ;; run hooks
  (run-hooks 'smg-mode-hook)

)

(defun smg-next-class ()
  (interactive)
  "Move to next class. Wrap around at end of buffer."
  (if (re-search-forward
       "\\(class\\)[ \t\n]+[^ \t\n{]+[ \t\n]*{"
       (point-max) t)
      (goto-char (match-end 1))
    (progn
      (goto-char (point-min))
      (if (re-search-forward
	   "\\(class\\)[ \t\n]+[^ \t\n{]+[ \t\n]*{"
	   (point-max) t)
	  (goto-char (match-end 1))
	(error "smg-next-class: no class found here !")))))

(defun smg-prev-class ()
  (interactive)
  "Move to previous class. Wrap around at beginning of buffer."
  (if (re-search-backward
       "\\(class\\)[ \t\n]+[^ \t\n{]+[ \t\n]*{"
       (point-min) t)
      (goto-char (match-beginning 1))
    (progn
      (goto-char (point-max))
      (if (re-search-backward
	   "\\(class\\)[ \t\n]+[^ \t\n{]+[ \t\n]*{"
	   (point-min) t)
	  (goto-char (match-beginning 1))
	(error "smg-prev-class: no class found here !")))))

(defun smg-class-name ()
  (interactive)
  (save-excursion
    (progn
      (end-of-line)
      (if (re-search-backward "class[ \t\n]*\\([^ \t\n{]*\\)+[ \t\n]*{" (point-min) t)
	  (match-string 1)
	(if (re-search-forward "class[ \t\n]*\\([^ \t\n{]*\\)+[ \t\n]*{" (point-max) t)
	    (match-string 1)
	  "")
      ))))

(defun smg-potential-class ()
  "Find a potential class name around current point"
  (save-excursion
    (skip-chars-backward "[^ \t\n]")
    (let ((start (point)))
      (cond
       ((re-search-forward "<:" (+ start 30) t)
	(re-search-forward (concat "[ \t\n]*\\([^ \t\n;]+\\)[ \t\n]*;"))
	(goto-char start)
	(match-string 1))
       (t (smg-class-name))
       )
      )
    )
  )

(defun smg-goto-class (&optional class)
  (interactive (list (read-string "class name: " (smg-potential-class))))
  "Goto to class in SMG file"
  (let ((where (point))
	(class (or class smg-focus-class)))
    (save-excursion
      (progn
	(goto-char (point-min))
	(if (re-search-forward (concat "class[ \t\n]+\\(" class "\\)[ \t\n]*{") (point-max) t)
	    (setq where (match-end 1))
	  (error (concat "smg-goto-class: " class " : not found")))
	)
      )
    (goto-char where)
    (unless (eq class smg-focus-class) (smg-focus class))
    )
  )

(defun smg-goto-focus ()
  (interactive)
  "Goto to focus class in SMG file"
  (smg-goto-class smg-focus-class)
  )

(defun smg-focus (&optional class)
  (interactive (list (read-string "class name: " (smg-class-name))))
  (let ((class (or class (smg-class-name))))
    (mg-say (concat "select focus " class)))
  )

(defun smg-focus-next-class ()
  (interactive)
  "Go and Focus next SMG class. Wrap around at end of buffer."
  (smg-next-class)
  (smg-focus)
  )

(defun smg-focus-prev-class ()
  (interactive)
  "Go and Focus next SMG class. Wrap around at beginning of buffer."
  (smg-prev-class)
  (smg-focus)
  )

(defun smg-focus-current ()
  (interactive)
  (smg-focus (smg-class-name))
  )

(defun smg-focus (&optional class)
  (interactive (list (read-string "class name: " (smg-class-name))))
  (let ((class (or class (smg-class-name))))
    (mg-say (concat "select focus " class)))
  )

(defun smg-next-selected ()
  (interactive)
  (mg-say "focus +"))

(defun smg-prev-selected ()
  (interactive)
  (mg-say "focus +"))

(defun smg-current-parents ()
  (interactive)
  (smg-parents))

(defun smg-parents (&optional class)
  (interactive (list (read-string "class name: " (smg-class-name))))
  (let ((class (or class (smg-class-name))))
    (mg-say (concat "select supers " class))
    (mg-say (concat "focus ."))
    )
  )

(defun smg-current-children ()
  (interactive)
  (smg-children))

(defun smg-children (&optional class)
  (interactive (list (read-string "class name: " (smg-class-name))))
  (let ((class (or class (smg-class-name))))
    (mg-say (concat "select children " class))
    (mg-say (concat "focus ."))
    )
  )


(defvar smg-mode-menu 
  (append
   '(

      ["Switch to MG" switch-to-mg t]
      ["MG Focus" mg-focus-current t]
      ["MG Refresh" mg-refresh t]
      ["XMLMG Focus" mg-goto-focus t]
      ["Next Class" mg-focus-next-class t]
      ["Prev Class" mg-focus-prev-class t]
      ["Parent Classes" mg-current-parents t]
      ["Children Classes" mg-current-children t]
      ["Next Selected" mg-next-selected t]
      ["Prev Selected" mg-prev-selected t]
      )
   mg-shared-menu
   )
  "The menu for editing XML Meta Grammar files"
  )


(easy-menu-define smg-mode-menu  
  smg-mode-map
  "Menu used in smg-mode."
  (append '("SMG") smg-mode-menu))



(defvar smg-buffer nil "*The current SMG buffer.")


(defvar smg-load-hook nil
  "*This hook is run when MG is loaded in.
This is a good place to put keybindings.")
	
(run-hooks 'smg-load-hook)

;;; Lastly, hooks:
;; =======================================================================

(defvar mg-load-hook nil
  "*This hook is run when MG is loaded in.
This is a good place to put keybindings.")
	
(run-hooks 'mg-load-hook)

(provide 'mg)

;;; mg.el ends here
