	mgtools

What is mgtools
===============

mgtools collects several useful tools and scripts to edit and
visualize Meta Grammars (MG)

	  smg2xml	to convert MG from a simple format (smg) to XML format
	  mg.el		an Emacs mode to edit MG (in both XML and SMG formats)
	  mg2smg.xsl	an XSLT stylesheet to convert from XML to SMG
	  mg*2html.xsl  several XSLT stylesheets to get HTML views on MG
	  mg2*.xsl	several XSLT stylesheets to convert to other formats
	  mgviewer	perl script to display a graph representation of a Meta Grammar


Copyright and Licence
=====================

INRIA 2003-2010

GNU Lesser Public License (LGPL)


Installation
============

See the INSTALL file for the compilation and installation procedure.


Documentation
=============

smg2xml
-------
smg2xml takes as input a meta-grammar in simplified format, and produces the
corresponding xml in the standard output.

Options to smg2xml:
	-c, --comments (default):
	Comments starting with ";;" are included as comment nodes in the xml.
	Other comments are ignored: starting with a simple ";", or within /*...*/
	in C style.

	-n, --no_comments:
	All comments are ignored.

	-w, --warning:
	Some checks are performed, and warnings are emitted when appropriate to stderr:
		* a class or macro may be defined only once;
		* a class or macro must be defined that is in use somewhere,
	must have a definition (classes are used in inheritance).
	Default: no check, no warning.

Try 
    smg2xml sample.smg


mgviewer
--------

	You can view mgviewer documentation typing

    perldoc mgviewer

	at a command prompt.


mg.el
-----
Emacs mode for smg.


Availability
============

- mgtools can be accessed by anonymous FTP:

	host:       ftp.inria.fr (192.93.2.54)
        directory:  INRIA/Projects/Atoll/Eric.Clergerie/TAG

- The latest version can also be retrieved from the Subversion repository on
  Inria Gforge typing the following command:

  svn checkout svn://scm.gforge.inria.fr/svn/mgkit/mgtools/trunk mgtools


Authors
=======

Send your bug reports and suggestions by E-mail to:

          Francois.Thomasset@inria.fr
	  Eric.De_La_Clergerie@inria.fr
