%{
#include <stdio.h>
#include <string.h>
#define YYSTYPE char*
extern long yylex();
extern unsigned yylineno;

#include "libxml/tree.h"
extern xmlDocPtr document_xml ;
extern xmlNodePtr MetaGrammar ;
extern xmlNodePtr CurrentClass ;
extern int class_found ;
extern int error_symb ;
extern int keep_comments;

#include "hash.h"
#include "symbol.h"

//#define DEBUGYACC 0
#ifdef DEBUGYACC
#define DBUGYACC(x) x
#else
#define DBUGYACC(x)
#endif

#define MALLOCERROR(xxxx) {printf("malloc_error:\t%s (line number: %d)\n",xxxx,yylineno);exit(-1);}

char * cp_str (char * str1, char * str2) {
  char * new_str ;
  if (str1 == NULL) return (str2) ;
  if (str2 == NULL) return (str1) ;
  new_str = (char*) calloc(strlen(str1)+strlen(str2)+1,sizeof (char)) ;
  sprintf (new_str, "%s%s", str1, str2) ;
  free (str1); free(str2) ;
  return (new_str) ;
}

char * xmlstrdup (char * in) {
    unsigned char *out;
    int ret,size,out_size,temp;
    xmlCharEncodingHandlerPtr handler;
    
    size = (long)strlen(in)+1; 
    out_size = size*2-1; 
    out = malloc((size_t)out_size); 

    if (out) {
        handler = xmlFindCharEncodingHandler("iso-8859-1") ;
        if (!handler) {
            free (out);
            out = NULL;
	    printf ("xmlstrdup: no handler\n") ; exit (1) ;
        }
    }
    if (out) {
      temp=size-1;
      ret = handler->input(out, &out_size, in, &temp);
      if ((ret < 0) || (temp-size+1)) {
	if (ret < 0) {
	  printf ("xmlstrdup: conversion wasn't successful.\n");
	}
	else {
	  printf ("xmlstrdup: conversion wasn't successful. converted: %i octets.\n", temp);
	}
	free (out);
	out = NULL;
	exit (1) ;
      }
      else {
	out = realloc(out,out_size+1); 
	out[out_size]=0; /*null terminating out*/
      }
    }
    else {
      MALLOCERROR ("xmlstrdup") ;
    }
    return (out);
}

xmlNodePtr path_add_at_end (xmlNodePtr path1, xmlNodePtr path2) 
{
    xmlNodePtr p1 = path1;
    for(;p1->children; p1 = p1->children);
    xmlAddChild(p1,xmlCopyNode(path2,1));
    return path1;
}

xmlNodePtr path_expand (xmlNodePtr path) 
{
    xmlNodePtr child = path->children;
    xmlNodePtr head = xmlCopyNode(path,2); /* don't copy children */
    if (child)
        child = path_expand(path->children);
    if (!xmlStrcmp(head->name,(const xmlChar *)"path")) {
            /* processing a macro */
        struct tbsymb_el *macro = new_path_macro_symb((char *)xmlGetProp(head,(const xmlChar*)"idref"),1,yylineno);
        if (macro != NULL) {
            xmlNodePtr value = (xmlNodePtr) macro->value;
            xmlNodePtr newhead=NULL;
            for(;value;value=value->next) {
                if (!newhead)
                    newhead = xmlCopyNode(value,1);
                else
                    xmlAddNextSibling(newhead,xmlCopyNode(value,1));
            }
            head = newhead;
        }
    }
    if (!child) {
        return head;
    } else if (head->next && child->next) {
        xmlNodePtr newpath = NULL;
        xmlNodePtr p = head;
        for (; p; p = p->next){
            xmlNodePtr c = child;
            for(; c; c = c->next){
                xmlNodePtr x = path_add_at_end(xmlCopyNode(p,1),c);
                if (!newpath)
                    newpath = x;
                else
                    xmlAddNextSibling(newpath,x);
            }
        }
        return newpath;
    } else if (head->next) {
        xmlNodePtr p = head;
        for (;p; p = p->next) {
            path_add_at_end(p,child);
        }
        return head;
    } else if (child->next) {
        xmlNodePtr newpath = NULL;
        for(;child; child = child->next) {
            xmlNodePtr p = path_add_at_end(xmlCopyNode(head,1),child);
            if (!newpath)
                newpath = p;
            else
                xmlAddNextSibling(newpath,p);
        }
        return newpath;
    } else
        return path;
}

int path_equal (xmlNodePtr p1, xmlNodePtr p2) 
{
    xmlNodePtr c1=p1->children;
    xmlNodePtr c2=p2->children;
    if (xmlStrcmp(p1->name,p2->name))
        return 0;
    if (xmlStrcmp(xmlGetProp(p1,(xmlChar *)"name"),xmlGetProp(p2,(xmlChar *)"name")))
        return 0;
    for(;c1 && c2; c1 = c1->next, c2 = c2->next) 
        if (!path_equal(c1,c2))
            return 0;
    if (c1 || c2)
        return 0;
    return 1;
}


xmlNodePtr path_reduce (xmlNodePtr path)
{
    xmlNodePtr p = path;
    int reduce=0;
    for(;p;p=p->next){
        if (xmlHasProp(p,(xmlChar*)"neg")) {
            xmlNodePtr p2=path;
            for (;p2;p2=p2->next) {
                if (path_equal(p2,p)){
                    xmlSetProp(p2,(xmlChar *)"delete",(xmlChar *)"yes");
                    reduce=1;
                }
            }
        }
    }
    if(reduce){
        xmlNodePtr new = NULL;
        for(p=path;p;p=p->next) {
            if (!xmlHasProp(p,(xmlChar *)"delete")){
                if (!new)
                    new = xmlCopyNode(p,1);
                else
                    xmlAddNextSibling(new,xmlCopyNode(p,1));
            }
        }
        path=new;
    }
    return path;
}

xmlNodePtr xmlAddChildAlt(xmlNodePtr e, xmlNodePtr c)
{
    xmlChar * ename = e->name;
    xmlChar * cname = c->name;
    if (!xmlStrcmp(ename,(const xmlChar *)"and")
        && !xmlStrcmp(cname,(const xmlChar *)"and")
        ) {
        xmlNodePtr p = c->children;
        for(; p; p = p->next) {
            xmlAddChild(e,p);
        }
    } else if (!xmlStrcmp(ename,(const xmlChar *)"guard")
               && !xmlStrcmp(cname,(const xmlChar *)"guard")
               ) {
        xmlNodePtr p = c->children;
        for(; p; p = p->next) {
            xmlAddChild(e,p);
        }
    } else {
        xmlAddChild(e,c);
    }
    return e;
}


xmlNodePtr neg_expand (xmlNodePtr e)
{
    xmlChar * name = e->name;
    if (!xmlStrcmp(name,(const xmlChar *)"and")) {
        xmlNodePtr guard = xmlNewNode(NULL,"guard");
        xmlNodePtr p = e->children;
        xmlNodePtr acc = xmlNewNode(NULL,"and");
        for(; p ; p = p->next) {
            xmlNodePtr nacc = xmlCopyNode(acc,1);
            xmlAddChildAlt(nacc,neg_expand(p));
            xmlAddChildAlt(guard,nacc);
            xmlAddChildAlt(acc,xmlCopyNode(p,1));
        }
        return guard;
    } else if (!xmlStrcmp(name,(const xmlChar *)"guard")) {
        xmlNodePtr and = xmlNewNode(NULL,"and");
        xmlNodePtr p = e->children;
        for(; p ; p = p->next) {
            xmlAddChildAlt(and,neg_expand(p));
        }
        return and;
    } else if (!xmlStrcmp(name,(const xmlChar *)"equation")
               && !xmlStrcmp(e->children->next->name,(const xmlChar*)"value")
               ) {
        xmlNodePtr equation = xmlNewNode(NULL,"equation");
        xmlAddChild(equation,xmlCopyNode(e->children,1));
        xmlAddChild(equation,neg_expand(e->children->next));
        return equation;
    } else if (!xmlStrcmp(name,(const xmlChar *)"value")) {
        xmlNodePtr value = xmlNewNode(NULL,"value");
        xmlAddChild(value,neg_expand(e->children));
        return value;
    } else if (!xmlStrcmp(name,(const xmlChar *)"not")) {
        return e->children;
    } else {
        xmlNodePtr neg = xmlNewNode(NULL,"not");
        xmlAddChild(neg,e);
        return neg;
    }
}



%}

%token CLASS DOCUMENTATION
%token IDENT MACRONAME TEMPLATE PATH DISABLE
%token LCURLYBK RCURLYBK LSQBK RSQBK LPAREN RPAREN
%token INHERIT FATHERREL
%token MINUS PLUS XPLUS STAR NEG PREC ANCESTOR
%token COMMA COLON SEMICOLON DOUBLECOLON EQUAL XEQUAL DOT ALT JOIN BANG ATMOST IMP EQUIV
%token DESC NODE OPT VALUE VAR FATHER DOMAIN PROPERTY
%left ALT
%left JOIN
%start Axiom
%%
Axiom: Dcllist Doc
             {
	       char * comment_str = $2 ;
	       if (comment_str && strlen (comment_str) > 0 && keep_comments) {
		 xmlNodePtr comment_node = xmlNewComment ((xmlChar*)comment_str) ;
		 xmlAddChild (MetaGrammar, comment_node) ;
	       }
	     } ;

Dcllist: Dcl {
             DBUGYACC(printf ("One Dcl\n") ;);
             }
       | Dcllist Dcl
       | error {printf ("\nLine number: %d\n",yylineno);} ;

Dcl: Doc CLASS IDENT
             {
	       char * comment_str = $1 ;
	       class_found += 1;
	       CurrentClass = xmlNewChild (MetaGrammar, NULL, (xmlChar*)"class", NULL) ;
	       xmlSetProp (CurrentClass, (xmlChar*)"name", (xmlChar*)$3) ;
	       new_class_symb ($3, 1, yylineno) ;
	       if (comment_str && strlen (comment_str) > 0 && keep_comments) {
		 xmlNodePtr comment_node = xmlNewComment ((xmlChar*)comment_str) ;
		 xmlAddChild (CurrentClass, comment_node) ;
	       }
	     }
     Doc
             {
	       char * comment_str = $5 ;
	       if (comment_str && strlen (comment_str) > 0 && keep_comments) {
		 xmlNodePtr comment_node = xmlNewComment ((xmlChar*)comment_str) ;
		 xmlAddChild (CurrentClass, comment_node) ;
	       }
	     }
     LCURLYBK Doc
             {
	       char * comment_str = $8 ;
	       if (comment_str && strlen (comment_str) > 0 && keep_comments) {
		 xmlNodePtr comment_node = xmlNewComment ((xmlChar*)comment_str) ;
		 xmlAddChild (CurrentClass, comment_node) ;
	       }
	     }
     Proplist RCURLYBK
             {
	       /* reinitialiser pour une autre classe */
	       CurrentClass = NULL ;
             }
   | Doc TEMPLATE MACRONAME
             {
	       new_template_macro_symb ($3, 1, yylineno) ;
             }
     EQUAL Value
             {
	       char * comment_str = $1 ;
	       xmlNodePtr templateNode = xmlNewNode (NULL, (xmlChar*)"vLib") ;
	       if (comment_str && strlen (comment_str) > 0 && keep_comments) {
		 xmlNodePtr comment_node = xmlNewComment ((xmlChar*)comment_str) ;
		 xmlAddChild (MetaGrammar, comment_node) ;
	       }
	       xmlSetProp (templateNode, (xmlChar*)"id", (xmlChar*)$3) ;
	       xmlAddChild (templateNode, (xmlNodePtr)$6) ;
	       xmlAddChild (MetaGrammar, templateNode) ;
             }
   | Doc PATH MACRONAME  EQUAL PathList
             {
	       struct tbsymb_el * symbol = new_path_macro_symb ($3, 1, yylineno) ;
	       char * comment_str = $1 ;
	       xmlNodePtr MacroNode = xmlNewNode (NULL, (xmlChar*)"path") ;
               xmlNodePtr path = (xmlNodePtr) $5;
               symbol->value = (char *)path;
	       if (comment_str && strlen (comment_str) > 0 && keep_comments) {
		 xmlNodePtr comment_node = xmlNewComment ((xmlChar*)comment_str) ;
		 xmlAddChild (MetaGrammar, comment_node) ;
	       }
	       xmlSetProp (MacroNode, (xmlChar*)"id", (xmlChar*)$3) ;
               while (path) {
                   xmlAddChild (MacroNode, xmlCopyNode(path,1)) ;
                   path = path->next;
               }
	       xmlAddChild (MetaGrammar, MacroNode) ;
             }
   | Doc DISABLE IDENT
             {
	       char * comment_str = $1 ;
	       xmlNodePtr DisableNode = xmlNewNode (NULL, (xmlChar*)"disable") ;
	       if (comment_str && strlen (comment_str) > 0 && keep_comments) {
		 xmlNodePtr comment_node = xmlNewComment ((xmlChar*)comment_str) ;
		 xmlAddChild (MetaGrammar, comment_node) ;
	       }
	       xmlSetProp (DisableNode, (xmlChar*)"name", (xmlChar*)$3) ;
	       xmlAddChild (MetaGrammar, DisableNode) ;
             }
   | error RCURLYBK {printf ("\nLine number: %d\n",yylineno);} ;

Proplist: 
        | Property Doc
             {
	       char * comment_str = $2 ;
	       if (comment_str && strlen (comment_str) > 0 && keep_comments) {
		 xmlNodePtr property_node = (xmlNodePtr)$1 ;
		 xmlNodePtr comment_node = xmlNewComment ((xmlChar*)comment_str) ;
		 DBUGYACC(printf ("Commentaire dans une propriete (1): %s comment_node=%o property_node=%o\n",
				  (xmlChar*)$2, comment_node, property_node););
		 xmlAddNextSibling (property_node, comment_node) ;
	       }
	     }
        | Property SEMICOLON Doc Proplist
             {
	       char * comment_str = $3 ;
	       if (comment_str && strlen (comment_str) > 0 && keep_comments) {
		 xmlNodePtr property_node = (xmlNodePtr)$1 ;
		 xmlNodePtr comment_node = xmlNewComment ((xmlChar*)comment_str) ;
		 DBUGYACC(printf ("Commentaire dans une propriete (liste): %s comment_node=%o property_node=%o Proplist=%o\n",
				  (xmlChar*)$3, comment_node, property_node, $4););
		 xmlAddNextSibling (property_node, comment_node) ;
	       }
	     }
        | error SEMICOLON {printf ("\nLine number: %d\n",yylineno);};

Property: Heritage
        | Pere
        | Ancetre
        | Precedence
        | Equality
        | Desc
        | Equation
        | Node
        | Needs
        | Provides
        | Guard
        | Atomic
        | NodeDisj
;

Heritage: INHERIT IDENT
             {
	       xmlNodePtr InheritNode = xmlNewChild (CurrentClass, NULL, (xmlChar*)"super", NULL) ;
	       DBUGYACC(printf ("Propriete Heritage: %s\n",$2););
	       xmlSetProp (InheritNode, (xmlChar*)"name", (xmlChar*)$2) ;
	       new_class_symb ($2, 0, yylineno) ;
	       $$ = (xmlChar*) InheritNode ;
             } ;

Pere: Idname FATHERREL Idname
             {
	       xmlNodePtr FatherNode = xmlNewChild (CurrentClass, NULL, (xmlChar*)"relation", NULL) ;
               DBUGYACC(printf ("Propriete Pere: %s %s\n",$1,$3););
	       xmlSetProp (FatherNode, (xmlChar*)"rel", (xmlChar*)"father") ;
	       xmlSetProp (FatherNode, (xmlChar*)"arg1", (xmlChar*)$1) ;
	       xmlSetProp (FatherNode, (xmlChar*)"arg2", (xmlChar*)$3) ;
	       $$ = (xmlChar*) FatherNode ;
             } ;

Ancetre: Idname ANCESTOR Idname
             {
	       xmlNodePtr AncestorNode = xmlNewChild (CurrentClass, NULL, (xmlChar*)"relation", NULL) ;
               DBUGYACC(printf ("Propriete Anc�tre: %s %s\n",$1,$3););
	       xmlSetProp (AncestorNode, (xmlChar*)"rel", (xmlChar*)"dominates") ;
	       xmlSetProp (AncestorNode, (xmlChar*)"arg1", (xmlChar*)$1) ;
	       xmlSetProp (AncestorNode, (xmlChar*)"arg2", (xmlChar*)$3) ;
	       $$ = (xmlChar*) AncestorNode ;
             } ;

Precedence: Idname PREC Idname
             {
	       xmlNodePtr PrecNode = xmlNewChild (CurrentClass, NULL, (xmlChar*)"relation", NULL) ;
	       DBUGYACC(printf ("Propriete Pr�c�dence: %s %s\n",$1,$3););
	       xmlSetProp (PrecNode, (xmlChar*)"rel", (xmlChar*)"precedes") ;
	       xmlSetProp (PrecNode, (xmlChar*)"arg1", (xmlChar*)$1) ;
	       xmlSetProp (PrecNode, (xmlChar*)"arg2", (xmlChar*)$3) ;
	       $$ = (xmlChar*) PrecNode ;
             } ;

Equality: Idname EQUAL Idname
             {
	       xmlNodePtr PrecNode = xmlNewChild (CurrentClass, NULL, (xmlChar*)"relation", NULL) ;
	       DBUGYACC(printf ("Propriete Equality: %s %s\n",$1,$3););
	       xmlSetProp (PrecNode, (xmlChar*)"rel", (xmlChar*)"equals") ;
	       xmlSetProp (PrecNode, (xmlChar*)"arg1", (xmlChar*)$1) ;
	       xmlSetProp (PrecNode, (xmlChar*)"arg2", (xmlChar*)$3) ;
	       $$ = (xmlChar*) PrecNode ;
             } ;

Desc: DESC LPAREN
      LSQBK Features RSQBK
      RPAREN
             {
	       xmlNodePtr DescNode = xmlNewChild (CurrentClass, NULL,
						  (xmlChar*)"description", NULL) ;
	       DBUGYACC(printf ("Propriete desc:%s %o\n",$4,$4););
	       xmlAddChild (DescNode, (xmlNodePtr) $4) ;
	       $$ = (xmlChar*) DescNode ;
             } ;

Features: 
             {
	       xmlNodePtr FSNode = xmlNewNode (NULL, (xmlChar*)"fs") ;
	       DBUGYACC(printf ("Features : liste vide %o\n", FSNode););
	       $$ = (char*) FSNode ;
             }
     | F
             {
	       xmlNodePtr FNode = (xmlNodePtr) $1 ;
	       xmlNodePtr FSNode = xmlNewNode (NULL, (xmlChar*)"fs") ;
	       DBUGYACC(printf ("Features : F %o %o\n", $1, FSNode););
	       xmlAddChild (FSNode, FNode) ;
	       $$ = (char*) FSNode ;
             }
     | Features COMMA F
             {
	       xmlNodePtr FNode = (xmlNodePtr) $3 ;
	       xmlNodePtr FSNode = (xmlNodePtr) $1 ;
	       DBUGYACC(printf ("Features : F FS %o %o %o\n", $3, $2, FSNode););
	       xmlAddChild (FSNode, FNode) ;
	       $$ = (char*) FSNode ;
             } ;

F: IDENT COLON Value
             {
	       xmlNodePtr FNode = xmlNewNode (NULL, (xmlChar*)"f") ;
	       DBUGYACC(printf ("Feature %s %o %o\n", $1, $3, FNode););
	       xmlSetProp (FNode, (xmlChar*)"name", (xmlChar*)$1) ;
	       xmlAddChild (FNode, (xmlNodePtr)$3) ;
	       $$ = (char*) FNode ;
             } ;


Guard: Idname IMP EqAlt
             { 
               xmlNodePtr GuardNode = (xmlNodePtr) $3;
               xmlSetProp (GuardNode, (xmlChar*)"rel", (xmlChar *) "+");
               xmlSetProp (GuardNode, (xmlChar*)"name", (xmlChar *) $1);
               xmlAddChild (CurrentClass, GuardNode) ;
	       DBUGYACC(printf("Guard: GuardNode=%o %s\n",GuardNode, $1););
	       $$ = (char*) GuardNode ;
             } 
      | NEG Idname IMP EqAlt
             { 
               xmlNodePtr GuardNode = (xmlNodePtr) $4;
               xmlSetProp (GuardNode, (xmlChar*)"rel", (xmlChar *) "-");
               xmlSetProp (GuardNode, (xmlChar*)"name", (xmlChar *) $2);
               xmlAddChild (CurrentClass, GuardNode) ;
	       DBUGYACC(printf("Guard: GuardNode=%o ~%s\n",GuardNode,$2););
	       $$ = (char*) GuardNode ;
             }
      | Idname PLUS EqAlt
            {
               xmlNodePtr GuardNode = (xmlNodePtr) $3;
               xmlNodePtr FailNode = xmlNewNode(NULL, (xmlChar*)"guard");
               xmlSetProp (GuardNode, (xmlChar*)"rel", (xmlChar *) "+");
               xmlSetProp (GuardNode, (xmlChar*)"name", (xmlChar *) $1);
               xmlAddChild (CurrentClass, GuardNode) ;
               xmlSetProp (FailNode, (xmlChar*)"rel", (xmlChar *) "-");
               xmlSetProp (FailNode, (xmlChar*)"name", (xmlChar *) $1);
               xmlAddChild (CurrentClass, FailNode) ;
	       DBUGYACC(printf("Guard: GuardNode=%o %s\n",GuardNode, $1););
	       $$ = (char*) GuardNode ;
             }
      | Idname XPLUS EqAlt
            {
               xmlNodePtr GuardNode = (xmlNodePtr) $3;
               xmlNodePtr FailNode = xmlNewNode(NULL, (xmlChar*)"guard");
               xmlSetProp (GuardNode, (xmlChar*)"rel", (xmlChar *) "++");
               xmlSetProp (GuardNode, (xmlChar*)"name", (xmlChar *) $1);
               xmlAddChild (CurrentClass, GuardNode) ;
               xmlSetProp (FailNode, (xmlChar*)"rel", (xmlChar *) "-");
               xmlSetProp (FailNode, (xmlChar*)"name", (xmlChar *) $1);
               xmlAddChild (CurrentClass, FailNode) ;
	       DBUGYACC(printf("Guard: GuardNode=%o %s\n",GuardNode, $1););
	       $$ = (char*) GuardNode ;
             }
      ;     

EqAlt: EqSeq
             { 
                 xmlNodePtr GuardNode = xmlNewNode (NULL, (xmlChar*)"guard");
                 xmlAddChild (GuardNode,(xmlNodePtr)$1);
		 DBUGYACC(printf("EqAlt: GuardNode=%o\n",GuardNode););
                 $$ = (char *)GuardNode;
             }
     | EqSeq IMP EqSeq ALT EqSeq
     {
         xmlNodePtr GuardNode = xmlNewNode (NULL, (xmlChar*)"guard");
         xmlNodePtr TrueNode = xmlNewNode(NULL,(xmlChar *)"and");
         xmlNodePtr FalseNode = xmlNewNode(NULL,(xmlChar *)"and");
         xmlNodePtr NegNode = neg_expand((xmlNodePtr)$1);
         xmlAddChildAlt(TrueNode,(xmlNodePtr)$1);
         xmlAddChildAlt(TrueNode,(xmlNodePtr)$3);
         xmlAddChildAlt(FalseNode,NegNode);
         xmlAddChildAlt(FalseNode,(xmlNodePtr)$5);
         xmlAddChildAlt(GuardNode,TrueNode);
         xmlAddChildAlt(GuardNode,FalseNode);
         $$ = (char *)GuardNode;
     }
     | EqSeq IMP EqSeq
     {
         xmlNodePtr GuardNode = xmlNewNode (NULL, (xmlChar*)"guard");
         xmlNodePtr TrueNode = xmlNewNode(NULL,(xmlChar *)"and");
         xmlNodePtr FalseNode = xmlNewNode(NULL,(xmlChar *)"and");
         xmlNodePtr NegNode = neg_expand((xmlNodePtr)$1);
         xmlAddChildAlt(TrueNode,(xmlNodePtr)$1);
         xmlAddChildAlt(TrueNode,(xmlNodePtr)$3);
         xmlAddChildAlt(FalseNode,NegNode);
         xmlAddChildAlt(GuardNode,TrueNode);
         xmlAddChildAlt(GuardNode,FalseNode);
         $$ = (char *)GuardNode;
     }
     | EqAlt ALT EqSeq
             {    xmlNodePtr GuardNode = (xmlNodePtr) $1;
	          xmlAddChild (GuardNode,(xmlNodePtr) $3);
		  DBUGYACC(printf("EqAlt: GuardNode=%o\n",GuardNode););
		  $$ = $1;
	     } ;

EqSeq: EqSeq1 { $$ = $1; }
     | EqSeq1 COMMA EqAlt1 {
         xmlNodePtr SeqGuardNode = (xmlNodePtr) $1;
         xmlAddChild (SeqGuardNode,(xmlNodePtr) $3);
	 DBUGYACC(printf("EqSeq: SeqGuardNode=%o\n",SeqGuardNode););
         $$ = $1;
     };

EqSeq1: EqAlt1 {
    xmlNodePtr SeqGuardNode = xmlNewNode (NULL, (xmlChar*)"and");
    xmlNodePtr seq = (xmlNodePtr) $1;
    for(;seq;seq = seq->next) 
        xmlAddChild (SeqGuardNode,seq);
    DBUGYACC(printf ("EqSeq1 (1): SeqGuardNode=%o %o\n", SeqGuardNode));
    $$ = (char *)SeqGuardNode;
             }
| EqSeq1 COMMA EqAlt1
{
    xmlNodePtr SeqGuardNode = (xmlNodePtr) $1;
    xmlNodePtr seq = (xmlNodePtr) $3;
    for(;seq; seq = seq->next)
        xmlAddChild (SeqGuardNode,seq);
    DBUGYACC(printf ("EqSeq1: SeqGuardNode=%o %o\n", SeqGuardNode, $3));
    $$ = (char *)SeqGuardNode;
};

EqAlt1: EqElem { $$ = $1; }
      | LPAREN EqAlt RPAREN
               {
                   $$ = $2;
               }

EqElem: EqTerm EQUAL EqTerm
             { 
               xmlNodePtr EqNode = xmlNewNode (NULL, (xmlChar*)"equation");
               xmlAddChild (EqNode, (xmlNodePtr) $1);
               xmlAddChild (EqNode, (xmlNodePtr) $3);
               $$ = (char *)EqNode;
	       DBUGYACC(printf ("EqElem: EqNode=%o\n", EqNode));
             }
      | EqTerm XEQUAL EqTerm
             { 
               xmlNodePtr EqNode = xmlNewNode (NULL, (xmlChar*)"equation");
               xmlSetProp (EqNode, (xmlChar*)"default", (xmlChar *) "+");
               xmlAddChild (EqNode, (xmlNodePtr) $1);
               xmlAddChild (EqNode, (xmlNodePtr) $3);
               $$ = (char *)EqNode;
	       DBUGYACC(printf ("EqElem: EqNode=%o\n", EqNode));
             }
      | EqElem PathComplete
      {
          xmlNodePtr EqNode = (xmlNodePtr) $1;
          xmlNodePtr allfeatures = (xmlNodePtr) $2;
          xmlNodePtr EqNodeList = NULL;
          for(;EqNode;EqNode=EqNode->next){
              xmlNodePtr features = allfeatures;
              for (;features;features=features->next) {
                  xmlNodePtr newEqNode = xmlCopyNode(EqNode,1);
                  xmlNodePtr child = newEqNode->children;
                  for (;child;child=child->next)
                      if (xmlStrcmp(child->name,(const xmlChar*)"value"))
                          path_add_at_end(child,features);
                  if (EqNodeList == NULL)
                      EqNodeList = newEqNode;
                  else
                      xmlAddNextSibling(EqNodeList,newEqNode);
              }
          }
          $$ = (char *) EqNodeList;
      }
      ;

Atomic: BANG EqTerm
             { 
               xmlNodePtr AtomicNode = xmlNewNode (NULL, (xmlChar*)"atomic");
               xmlAddChild (AtomicNode, (xmlNodePtr) $2);
               xmlAddChild (CurrentClass, AtomicNode) ;
               $$ = (char *)AtomicNode;
             } ;

Equation: EqTerm EQUAL EqTerm PathComplete
             {
	       xmlNodePtr Root1 = (xmlNodePtr)$1 ;
	       xmlNodePtr Root2 = (xmlNodePtr)$3 ;
               xmlNodePtr features = (xmlNodePtr)$4;
               xmlNodePtr EquationNode;
               while (features != NULL) {
                   xmlNodePtr newRoot1 = xmlCopyNode(Root1,1);
                   xmlNodePtr newRoot2 = xmlCopyNode(Root2,1);
                   EquationNode = xmlNewNode (NULL, (xmlChar*)"equation") ;
                   xmlAddChild (EquationNode, newRoot1) ;
                   xmlAddChild (EquationNode, newRoot2) ;
                   xmlAddChild (CurrentClass, EquationNode) ;
                   if (xmlStrcmp(newRoot1->name,(const xmlChar *)"value")) {
                       while (newRoot1->children != NULL) {
                           newRoot1 = newRoot1->children;
                       }                       
                       xmlAddChild(newRoot1,xmlCopyNode(features,1));
                   }
                   if (xmlStrcmp(newRoot2->name,(const xmlChar *)"value")) {
                       while (newRoot2->children != NULL) {
                           newRoot2 = newRoot2->children;
                       }
                       xmlAddChild(newRoot2,xmlCopyNode(features,1));
                   }
                   features = features->next;
               }
               $$ = (char *)EquationNode ;
             } 
        |
           EqTerm EQUAL EqTerm
             {
	       xmlNodePtr EquationNode = xmlNewNode (NULL, (xmlChar*)"equation") ;
	       xmlNodePtr Root1 = (xmlNodePtr)$1 ;
	       xmlNodePtr Root2 = (xmlNodePtr)$3 ;
	       xmlAddChild (EquationNode, Root1) ;
	       xmlAddChild (EquationNode, Root2) ;
	       xmlAddChild (CurrentClass, EquationNode) ;
	       DBUGYACC(printf ("Propriete Equation : Equation_node=%o Root1=%o Root2=%o\n", EquationNode, $1, $3););
               $$ = (char *)EquationNode ;
             } ;

PathListElem: LCURLYBK PathList RCURLYBK
{
    $$ = (char *) path_reduce((xmlNodePtr)$2);
}

PathComplete: PathListElem
{
    $$ = $1;
}
           | PathComplete PathListElem
           {
               xmlNodePtr PathList1 = (xmlNodePtr) $1;
               xmlNodePtr PathList2 = (xmlNodePtr) $2;
               xmlNodePtr NewPathList = NULL;
               while(PathList1) {
                   xmlNodePtr path2 = PathList2;
                   while (path2) {
                       xmlNodePtr newPath = xmlCopyNode(PathList1,1);
                       if (NewPathList == NULL)
                           NewPathList = newPath;
                       else
                           xmlAddNextSibling(NewPathList,newPath);
                       while (newPath->children != NULL) {
                           newPath = newPath->children;
                       }
                       xmlAddChild(newPath,xmlCopyNode(path2,1));
                       path2 = path2->next;
                   }
                   PathList1 = PathList1->next;
               }
               $$ = (char *)NewPathList;
           }

        
    
XPath: Path
    {
        $$ = (char *) path_expand((xmlNodePtr)$1);
    }
     | NEG Path
     {
         xmlNodePtr PathNodeList = path_expand((xmlNodePtr) $2);
         xmlNodePtr p = PathNodeList;
         for(;p;p=p->next)
             xmlSetProp(p,(xmlChar*)"neg",(xmlChar*)"yes");
         $$ = (xmlChar *) PathNodeList;
     }
;

PathList: XPath
{
    $$ = $1;
}
     | PathList COMMA XPath
{
    xmlNodePtr PathNodeList = (xmlNodePtr) $1;
    xmlNodePtr PathNodeList2 = (xmlNodePtr) $3;
    for (; PathNodeList2; PathNodeList2 = PathNodeList2->next)
        xmlAddNextSibling(PathNodeList,xmlCopyNode(PathNodeList2,1));
    $$ = (char *) PathNodeList;
}

;

NodeDisj : BANG NodeDisjAux {
    xmlNodePtr NodeDisj = (xmlNodePtr) $2;
    xmlSetProp (NodeDisj, (xmlChar*) "match",(xmlChar*) "exact");
    xmlAddChild (CurrentClass, NodeDisj) ;
    $$ = (char*) $2;
             }
         | ATMOST NodeDisjAux
         {
    xmlNodePtr NodeDisj = (xmlNodePtr) $2;
    xmlSetProp (NodeDisj, (xmlChar*) "match",(xmlChar*) "atmost");
    xmlAddChild (CurrentClass, NodeDisj) ;
    $$ = (char*) $2;
         }
;

NodeDisjAux : NodeName
    {
        xmlNodePtr NodeDisj = xmlNewNode (NULL, (xmlChar*)"nodedisj");
        xmlAddChild (NodeDisj,(xmlNodePtr) $1);
        $$ = (char *) NodeDisj;
    }
    | NodeDisjAux ALT NodeName
    {
        xmlNodePtr NodeDisj = (xmlNodePtr) $1;
        xmlAddChild (NodeDisj,(xmlNodePtr) $3);
        $$ = (char*) $1;
    };

NodeName : Idname {
    xmlNodePtr Node = xmlNewNode (NULL, (xmlChar*)"node");
    xmlSetProp (Node, (xmlChar*) "name",(xmlChar*) $1);
    $$ = (char*) Node;
}
;


EqTerm: RootOfPath Path
             {
	       xmlNodePtr RootNode = (xmlNodePtr)$1 ;
	       xmlAddChild (RootNode, (xmlNodePtr)$2) ;
	       DBUGYACC(printf ("EqTerm: RootNode:%o Path:%o\n",RootNode,$2););
	       $$ = (char*) RootNode ;
             }
    | VALUE LPAREN Value RPAREN
             {
	       xmlNodePtr Node = xmlNewNode (NULL, (xmlChar*)"value") ;
	       xmlAddChild (Node, (xmlNodePtr)$3) ;
	       DBUGYACC(printf ("EqTerm: Node:%o Value:%o\n",Node,$3););
	       $$ = (char*) Node ;
             }
    | DOMAIN LPAREN IDENT RPAREN
             {
	       xmlNodePtr Node = xmlNewNode (NULL, (xmlChar*)"domain") ;
               xmlSetProp (Node, (xmlChar*)"value", (xmlChar*)$3) ;
	       DBUGYACC(printf ("EqTerm: Node:%o Value:%o\n",Node,$3););
	       $$ = (char*) Node ;
             }
    | PROPERTY LPAREN IDENT RPAREN
             {
	       xmlNodePtr Node = xmlNewNode (NULL, (xmlChar*)"property") ;
               xmlSetProp (Node, (xmlChar*)"value", (xmlChar*)$3) ;
	       DBUGYACC(printf ("EqTerm: Node:%o Value:%o\n",Node,$3););
	       $$ = (char*) Node ;
             }

;

Varname: VAR Idname
           {
	       xmlNodePtr VarNode = xmlNewNode (NULL, (xmlChar*)"var") ;
	       xmlSetProp (VarNode, (xmlChar*)"name", (xmlChar*)$2) ;
	       $$ = (char*) VarNode ;
           } ;

AtomAlts: Atom
             {
                 xmlNodePtr AltNode = xmlNewNode (NULL, (xmlChar*)"vAlt") ;
                 xmlAddChild (AltNode, (xmlNodePtr) $1) ;
                 $$ = (char*) AltNode ;
	     }
        | Atom ALT AtomAlts
             {
	       xmlNodePtr AltNode = (xmlNodePtr)$3 ;
	       xmlNodePtr FirstAltNode = AltNode->xmlChildrenNode ;
	       xmlAddPrevSibling (FirstAltNode, (xmlNodePtr)$1) ;
	       $$ = (char*) AltNode ;
	     } ;

Atom: IDENT {
	       xmlNodePtr VarNode = xmlNewNode (NULL, (xmlChar*)"symbol") ;
	       xmlSetProp (VarNode, (xmlChar*)"value", (xmlChar*)$1) ;
	       $$ = (char*) VarNode ;
	     }

     | STAR
             {
	       xmlNodePtr VarNode = xmlNewNode (NULL, (xmlChar*)"symbol") ;
	       xmlSetProp (VarNode, (xmlChar*)"value", (xmlChar*)"*") ;
	       $$ = (char*) VarNode ;
	     }
     | PLUS
             {
	       $$ = (char*) xmlNewNode (NULL, (xmlChar*)"plus") ;
	     }
     | MINUS
             {
	       $$ = (char*) xmlNewNode (NULL, (xmlChar*)"minus") ;
	     } ;

NValue:  Atom { $$ = $1; }
     | Atom ALT AtomAlts
             {
	       xmlNodePtr AltNode = (xmlNodePtr)$3 ;
	       xmlNodePtr FirstAltNode = AltNode->xmlChildrenNode ;
	       xmlAddPrevSibling (FirstAltNode, (xmlNodePtr) $1) ;
	       $$ = (char*) AltNode ;
	     } ;

Value: NValue { $$ = $1; }
     | NEG NValue
             {
	       xmlNodePtr NotNode = xmlNewNode (NULL, (xmlChar*)"not") ;
               xmlAddChild(NotNode, (xmlNodePtr)$2);
	       $$ = (char*) NotNode ;
	     }
     | Varname
             {
	       $$ = $1;
	     }
     | Varname JOIN Value
             {
                 xmlNodePtr VarNode = (xmlNodePtr) $1;
                 xmlAddChild (VarNode, (xmlNodePtr)$3) ;
                 $$ = (char*) VarNode ;
	     }
     | MACRONAME
            {
	       xmlNodePtr FNode = xmlNewNode (NULL, (xmlChar*)"fs") ;
	       xmlSetProp (FNode, (xmlChar*)"idref", (xmlChar*)$1) ;
	       $$ = (char*) FNode ;
             }
     | LSQBK Features RSQBK
             {
	       $$ = (char*) $2 ;
             }
     |  LPAREN IDENT RPAREN LSQBK Features RSQBK
             {
                 xmlSetProp ((xmlNodePtr)$5, (xmlChar *) "type", (xmlChar *) $2);
                 $$ = (char*) $5 ;
             } 
     ;

Node: VNode
     | OPT VNode
            {
	      xmlNodePtr Node = (xmlNodePtr) $2 ;
	      xmlSetProp (Node, (xmlChar*)"optional", (xmlChar*)"yes") ;
	      $$ = (char*) Node ;
	    } ;

VNode: RawNode
            {
	      xmlNodePtr Node = (xmlNodePtr) $1 ;
	      xmlSetProp (Node, (xmlChar*)"virtual", (xmlChar*)"yes") ;
	      $$ = (char*) Node ;
	    }
     | NODE RawNode
            {
	      xmlNodePtr Node = (xmlNodePtr) $2 ;
	      xmlSetProp (Node, (xmlChar*)"virtual", (xmlChar*)"no") ;
	      $$ = (char *) Node ;
	    } ;

RawNode: Idname COLON Value
           {
	       xmlNodePtr Node = xmlNewNode (NULL, (xmlChar*)"node") ;
	       DBUGYACC (printf ("Node %s\n",$1););
	       xmlSetProp (Node, (xmlChar*)"name", (xmlChar*)$1) ;
//               xmlSetProp (Node, (xmlChar*)"virtual", (xmlChar*)"yes") ;
	       xmlAddChild (Node, (xmlNodePtr)$3) ;
	       xmlAddChild (CurrentClass, Node) ;
               $$= (char *) Node;
           } ;

Path: 
             {
	       xmlNodePtr PathNode = NULL ;
	       $$ = (char*) PathNode ;
	     }
    | DOT IDENT Path 
             {
	       xmlNodePtr PathNode = xmlNewNode (NULL, (xmlChar*)"f") ;
	       xmlSetProp (PathNode, (xmlChar*)"name", (xmlChar*)$2) ;
	       if ($3 != NULL) {
		 xmlAddChild (PathNode, (xmlNodePtr)$3) ;
	       }
	       $$ = (char*) PathNode ;
	     } 
    | DOT MACRONAME Path
             {
	       xmlNodePtr PathNode = xmlNewNode (NULL, (xmlChar*)"path") ;
	       xmlSetProp (PathNode, (xmlChar*)"idref", (xmlChar*)$2) ;
	       if ($3 != NULL) {
		 xmlAddChild (PathNode, (xmlNodePtr)$3) ;
	       }
	       new_path_macro_symb ($2, 0, yylineno) ;
	       $$ = (char*) PathNode ;
	     } ;

RootOfPath : DESC
             {
	       xmlNodePtr Node = xmlNewNode (NULL, (xmlChar*)"description") ;
	       $$ = (char*) Node ;
	     }
    | FATHER LPAREN Idname RPAREN
             {
	       xmlNodePtr Node = xmlNewNode (NULL, (xmlChar*)"father") ;
	       xmlSetProp (Node, (xmlChar*)"name", (xmlChar*)$3) ;
	       $$ = (char*) Node ;
	     }
    | NODE LPAREN Idname RPAREN
             {
	       xmlNodePtr Node = xmlNewNode (NULL, (xmlChar*)"node") ;
	       xmlSetProp (Node, (xmlChar*)"name", (xmlChar*)$3) ;
	       $$ = (char*) Node ;
	     }
   | Varname
             { $$ = $1; } ;

Needs: MINUS Idname
             {
	       xmlNodePtr Node = xmlNewChild (CurrentClass, NULL, (xmlChar*)"needs", NULL) ;
	       DBUGYACC(printf ("Propriete Needs:%s\n",$1););
	       xmlSetProp (Node, (xmlChar*)"name", (xmlChar*)$2) ;
	       $$ = (char*) Node ;
             } ;

Provides: PLUS Idname
             {
	       xmlNodePtr Node = xmlNewChild (CurrentClass, NULL, (xmlChar*)"provides", NULL) ;
	       DBUGYACC(printf ("Propriete Provides:%s\n",$1););
	       xmlSetProp (Node, (xmlChar*)"name", (xmlChar*)$2) ;
	       $$ = (char*) Node ;
             } ;

Idname: IDENT
             {
	       $$ = (char*) $1 ;
             }
    | Idname DOUBLECOLON IDENT
             {
	       char * str1 = (char*) $1 ;
	       char * str2 = (char*) $3 ;
	       char * newstr = (char*) calloc (strlen(str1) + 2 + strlen(str2),
					       sizeof (char)) ;
	       sprintf (newstr, "%s:%s", str1, str2) ;
	       $$ = newstr ;
             } ;

Doc: { $$ = NULL ; }
| DOCUMENTATION Doc
             {
	       char * str1 = $1; char * str2 = $2 ;
	       DBUGYACC(printf ("Doc:\t%s\n", $1));
	       $$ = cp_str (str1,str2) ;
             } ;
