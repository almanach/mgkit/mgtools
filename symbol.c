/* Symbol table management */
#define NULL 0
#include <stdlib.h>
#include <stdio.h>
#include "hash.h"
#include "symbol.h"
int error_symb = 0 ;
extern int warning_flag ;
struct tbsymb_el* new_tbsymb_el () {
  struct tbsymb_el * x = (struct tbsymb_el*) malloc (sizeof (struct tbsymb_el)) ;
  if (!x) {
    printf ("ERROR: cannot allocate memory in new_tbsymb_el\n") ; exit(1) ;
  }
  x->defs = NULL ; x->uses = NULL ; x->symbol_type = undef_symbol ;
  return (x) ;
}

struct tbsymb_el* new_class_symb (char* name, int isdef, int line_num) {
  struct tbsymb_el* x;
  x = (struct tbsymb_el*) lookup (name) ;
  if (!x) {
    x = new_tbsymb_el() ;
    x->symbol_type = class_symbol ;
    insert (name,x) ;
  }
  else {
    if (isdef && x->defs) {
      if (warning_flag) {
	fprintf (stderr, "Class %s is defined at line %d,\n",
		 name, line_num) ;
	fprintf (stderr, "but I have already seen at least one previous definition at line(s): ") ;
	prt_list (stderr, x->defs) ;
	fprintf (stderr, "\n") ;
      }
      error_symb++ ;
    }
  }
  if (isdef) add_def (x,line_num) ;
  else add_use (x,line_num) ;
  return (x) ;
}

void prt_symb_t (FILE *f, symb_t ty) {
  switch (ty) {
  case class_symbol: fprintf (f,"Class") ; break ;
  case template_macro_symbol: fprintf (f,"template macro") ; break ;
  case path_macro_symbol: fprintf (f,"path macro") ; break ;
  default:  fprintf (f,"Undef") ; break ;
  }
}

struct tbsymb_el* new_template_macro_symb (char* name, int isdef, int line_num) {
  struct tbsymb_el* x;
  x = (struct tbsymb_el*) lookup (name) ;
  if (x) {
    if (x->symbol_type != template_macro_symbol) {
	if (warning_flag) {
	  if (isdef) {
	    fprintf (stderr,
		     "At line %d: Definition of symbol %s as a template macro\n",
		     line_num, name) ;
	    fprintf (stderr, "Already known as: ") ;
	    prt_symb_t (stderr, x->symbol_type) ;
	    fprintf (stderr, "\n") ;
	  }
	  else {
	    fprintf (stderr,
		     "At line %d: Use of symbol %s as a template macro\n",
		     line_num, name) ;
	    fprintf (stderr, "Already known as: ") ;
	    prt_symb_t (stderr, x->symbol_type) ;
	    fprintf (stderr, "\n") ;
	  }
	  fprintf (stderr, "Known definitions of %s at this step: ",name) ;
	  prt_list (stderr, x->defs) ; fprintf(stderr, "\n") ;
	  fprintf (stderr, "Known uses of %s at this step: ",name) ;
	  prt_list (stderr, x->uses) ; fprintf(stderr, "\n") ;
      }
      error_symb++ ;
    }
  }
  else {
    x = new_tbsymb_el() ;
    x->symbol_type = template_macro_symbol ;
    insert (name,x) ;
  }
  if (isdef) add_def (x,line_num) ;
  else add_use (x,line_num) ;
  return (x) ;
}

struct tbsymb_el* new_path_macro_symb (char* name, int isdef, int line_num) {
  struct tbsymb_el* x;
  x = (struct tbsymb_el*) lookup (name) ;
  if (x) {
    if (x->symbol_type != path_macro_symbol) {
      if (warning_flag) {
	if (isdef) {
	  fprintf (stderr,
		   "Definition of symbol %s as a path macro (line %d)\n",
		   name, line_num) ;
	  fprintf (stderr, "Already known as: ") ;
	  prt_symb_t (stderr, x->symbol_type) ;
	  fprintf (stderr, "\n") ;
	}
	else {
	  fprintf (stderr,
		   "Use of symbol %s as a template macro (line %d)\n",
		   name, line_num) ;
	  fprintf (stderr, "Already known as: ") ;
	  prt_symb_t (stderr, x->symbol_type) ;
	  fprintf (stderr, "\n") ;
	}
	fprintf (stderr, "Known definitions of %s at this step: ",name) ;
	prt_list (stderr, x->defs) ;
	fprintf(stderr, "\n") ;
	fprintf (stderr, "Known uses of %s at this step: ",name) ;
	prt_list (stderr, x->uses) ;
	fprintf(stderr, "\n") ;
      }
      error_symb++ ;
    }
  }
  else {
    x = new_tbsymb_el() ;
    x->symbol_type = path_macro_symbol ;
    insert (name,x) ;
  }
  if (isdef) add_def (x,line_num) ;
  else add_use (x,line_num) ;
  return (x) ;
}

struct int_list* new_int_list () {
  struct int_list * x = (struct int_list*) malloc (sizeof (struct int_list )) ;
  if (!x) {
    printf ("ERROR: cannot allocate memory in new_int_list\n") ; exit(1) ;
  }
  x->val = 0 ; x->next = NULL ;
  return x;
}

struct int_list* add_to_list (struct int_list* mylist, int x) {
  if (mylist == NULL) {
    mylist = new_int_list () ;
    mylist->val = x ;
  }
  else {
    struct int_list* the_list = mylist ;
    while (the_list->next) { the_list = the_list->next ; }
    the_list->next = new_int_list () ;
    the_list->next->val = x ;
  }
  return (mylist) ;
}

void prt_list (FILE* f, struct int_list* mylist) {
  struct int_list* the_list = mylist ;
  if (mylist == NULL) return ; 
  while (the_list) {
    fprintf (f, "%d ", the_list->val) ;
    the_list = the_list->next ;
  }
}

void add_def (struct tbsymb_el* t, int x) {
  t->defs = add_to_list (t->defs, x) ;
}

void add_use (struct tbsymb_el* t, int x) {
  t->uses = add_to_list (t->uses, x) ;
}
