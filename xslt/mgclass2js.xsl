<?xml version="1.0"?>
<!-- Author: Eric de la Clergerie "Eric.De_La_Clergerie@inria.fr" -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>

<xsl:output
  method="html"
  indent="yes"
  encoding="ISO-8859-1"/>

<xsl:template match="/metaGrammar">
   <html>
    <head>
      <link rel="STYLESHEET" type="text/css" href="style.css"/>
      <title>Classes</title>
      <script language="JavaScript" src="cooltree.js"/>
      <script language="JavaScript" src="class_format.js"/>
    </head>
    <body>
      <script language="JavaScript">
        var CLASS_NODES = [
        <xsl:for-each select="class[not(supers)]">
          <xsl:sort select="@name"/>
          <xsl:apply-templates select="."/>
          <xsl:if test="position() != last()">,</xsl:if>
        </xsl:for-each>
        ];
        var tree = new COOLjsTree ("classes", CLASS_NODES, CLASS_FORMAT);
        tree.expandNode(0);
      </script>
    </body>
  </html>
</xsl:template>

<xsl:template match="class">
  <xsl:variable name="name" select="@name"/>
  [ '<xsl:value-of select="@name"/>', null, null,

  ['provides', null, null
  <xsl:for-each select="provides/pf">
    <xsl:sort select="@name"/>
    , ['<xsl:value-of select="@name"/>', null, null]
  </xsl:for-each>
  ],

  ['needs', null, null
  <xsl:for-each select="needs/pf">
    <xsl:sort select="@name"/>
    , ['<xsl:value-of select="@name"/>', null, null]
  </xsl:for-each>
  ],

  ['nodes', null, null
  <xsl:for-each select="content/node">
    <xsl:sort select="@name"/>
    , ['<xsl:value-of select="@name"/>', null, null]
  </xsl:for-each>
  ]

  <xsl:variable name="next" 
    select="/metaGrammar/class[supers[s[@name = $name]]]"/>
  <xsl:for-each select="$next">
    <xsl:sort select="@name"/>
    , <xsl:apply-templates select="."/>
  </xsl:for-each>

  ]
</xsl:template>

</xsl:stylesheet>
