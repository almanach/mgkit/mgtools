<?xml version="1.0"?>
<!-- Author: Eric de la Clergerie "Eric.De_La_Clergerie@inria.fr" -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>

<xsl:output
  method="html"
  indent="yes"
  encoding="ISO-8859-1"/>

<xsl:template match="/metaGrammar">
   <html>
    <head>
      <link rel="STYLESHEET" type="text/css" href="style.css"/>
      <title>Classes</title>
    </head>
    <body>
      <xsl:for-each select="class">
        <xsl:sort select="@name"/>
        <xsl:apply-templates select="."/>        
      </xsl:for-each>
    </body>
  </html>
</xsl:template>

<xsl:template match="class">

  <xsl:variable name="name" select="@name"/>

  <a name="{$name}"/>
  <h1 class="class"><xsl:value-of select="$name"/></h1>

  <h2 class="class">Up Classes</h2>

  <ul class="class">
    <xsl:for-each select="super">
      <xsl:sort select="@name"/>
      <li class="class"><a href="#{@name}"><xsl:value-of select="@name"/></a></li>
    </xsl:for-each>
  </ul>

  <h2 class="class">Down Classes</h2>

  <ul>
    <xsl:for-each select="/metaGrammar/class/super[@name = $name]">
      <xsl:sort select="../@name"/>
      <li class="class"><a href="#{../../@name}"><xsl:value-of select="../@name"/></a></li>
    </xsl:for-each>
  </ul>

  <h2 class="resource">Provides</h2>

  <ul>
    <xsl:for-each select="provides">
      <xsl:sort select="@name"/>
      <li class="resource"><a href="resources.html#{@name}" target="resources"><xsl:value-of select="@name"/></a></li>
    </xsl:for-each>
  </ul>

  <h2 class="resource">Needs</h2>

  <ul>
    <xsl:for-each select="needs">
        <xsl:sort select="@name"/>
      <li class="resource"><a href="resources.html#{@name}" target="resources"><xsl:value-of select="@name"/></a></li>
    </xsl:for-each>
  </ul>

  <h2 class="node">Nodes</h2>

  <ul>
    <xsl:for-each select="node">
      <xsl:sort select="@name"/>
      <li class="node"><a href="nodes.html#{@name}" target="nodes"><xsl:value-of select="@name"/></a></li>
    </xsl:for-each>
  </ul>

</xsl:template>

</xsl:stylesheet>
