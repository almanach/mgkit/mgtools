<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:exslt="http://exslt.org/common"
  xmlns:func="http://exslt.org/func"
>

<xsl:output
        method="text"
        indent="no"
        encoding="ISO-8859-1"/>

<xsl:strip-space elements="*"/>

<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
<xsl:variable name="plquote" select="concat($uppercase,'_-$%:,;!?.��()[]�� ')"/>
<xsl:variable name="squote"><xsl:text>'</xsl:text></xsl:variable>
<xsl:variable name="dquote"><xsl:text>"</xsl:text></xsl:variable>

<xsl:template match="@*" mode="plquote">
  <xsl:call-template name="plquote">
    <xsl:with-param name="token" select="."/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="plquote">
  <xsl:param name="token"/>
  <xsl:choose>
    <xsl:when test="number($token)">
      <xsl:value-of select="$token"/>
    </xsl:when>
    <xsl:when test="$token = $squote">
      <xsl:text>''''</xsl:text>
    </xsl:when>
    <xsl:when test="$token = $dquote">
      <xsl:text>'"'</xsl:text>
    </xsl:when>
    <xsl:when test="$token != translate($token,$plquote,'x')">
      <xsl:text>'</xsl:text><xsl:value-of select="$token"/><xsl:text>'</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$token"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="metaGrammar">
  <xsl:apply-templates select="disable"/>
  <xsl:apply-templates select="class"/>
</xsl:template>

<xsl:template match="disable">
  <xsl:text>disabled_class(</xsl:text>
  <xsl:apply-templates select="@name"/>
  <xsl:text>).

</xsl:text>
</xsl:template>

<xsl:template match="class">
  <xsl:variable name="class"><xsl:apply-templates select="@name"/></xsl:variable>
  <xsl:text>class(</xsl:text>
  <xsl:value-of select="$class"/>
  <xsl:text>).

</xsl:text>
<xsl:apply-templates>
  <xsl:with-param name="class" select="$class"/>
</xsl:apply-templates>
</xsl:template>

<xsl:template match="super">
  <xsl:param name="class"/>
  <xsl:text>super(</xsl:text>
  <xsl:value-of select="$class"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="@name"/>
  <xsl:text>).

</xsl:text>
</xsl:template>

<xsl:template match="provides[@name]|needs[@name]">
  <xsl:param name="class"/>
  <xsl:value-of select="name(.)"/>
  <xsl:text>(</xsl:text>
  <xsl:value-of select="$class"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="@name"/>
  <xsl:text>).

</xsl:text>
</xsl:template>

<xsl:template match="description[*]">
  <xsl:param name="class"/>
    <xsl:text>description(</xsl:text>
    <xsl:value-of select="$class"/>
    <xsl:text>,</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>).

</xsl:text>
</xsl:template>

<xsl:template match="nodedisj">
  <xsl:param name="class"/>
  <xsl:text>nodedisj(</xsl:text>
    <xsl:value-of select="$class"/>
    <xsl:text>,</xsl:text>
<!--    <xsl:value-of select="generate-id(.)"/>
    <xsl:text>,</xsl:text> -->
    <xsl:value-of select="@match"/>
    <xsl:text>,[</xsl:text>
    <xsl:for-each select="node">
      <xsl:apply-templates select="@name"/>
      <xsl:if test="not(position()=last())">
	<xsl:text>,</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>]).

</xsl:text>
</xsl:template>

<xsl:template match="node">
  <xsl:param name="class"/>
  <xsl:variable name="node"><xsl:apply-templates select="@name"/></xsl:variable>
  <xsl:if test="@virtual != 'yes'">
    <xsl:text>node(</xsl:text>
    <xsl:value-of select="$class"/>
    <xsl:text>,</xsl:text>
    <xsl:value-of select="$node"/>
    <xsl:text>).

</xsl:text>
  </xsl:if>
  <xsl:if test="@optional = 'yes'">
optional(<xsl:value-of select="$class"/>,<xsl:value-of select="$node"/>).
  </xsl:if>
  <xsl:if test="*">
    <xsl:text>nodefeature(</xsl:text>
    <xsl:value-of select="$class"/>
    <xsl:text>,
    </xsl:text>
    <xsl:value-of select="$node"/>
    <xsl:text>,
    </xsl:text>
    <xsl:apply-templates/>
    <xsl:text>
).

</xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="relation">
  <xsl:param name="class"/>
  <xsl:value-of select="@rel"/>
  <xsl:text>(</xsl:text>
  <xsl:value-of select="$class"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="@arg1"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="@arg2"/>
  <xsl:text>).

</xsl:text>
</xsl:template>

<xsl:template match="symbol[@value]">
  <xsl:apply-templates select="@value" mode="plquote"/>
</xsl:template>

<xsl:template match="fs">
  <xsl:if test="@type">
    <xsl:apply-templates select="@type" mode="plquote"/>
    <xsl:text> @ </xsl:text>
  </xsl:if>
  <xsl:text>[</xsl:text>
  <xsl:for-each select="f">
    <xsl:apply-templates select="."/>
    <xsl:if test="not(position()=last())">
      <xsl:text>,
      </xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text>]</xsl:text>
</xsl:template>

<xsl:template match="plus">
  <xsl:text>+</xsl:text>
</xsl:template>

<xsl:template match="minus">
  <xsl:text>-</xsl:text>
</xsl:template>

<xsl:template match="f[*]">
  <xsl:apply-templates select="@name" mode="plquote"/>
  <xsl:text>: </xsl:text>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="f[@fVal]">
  <xsl:apply-templates select="@name" mode="plquote"/>
  <xsl:text>: </xsl:text>
  <xsl:variable name="id" select="@fVal"/>
  <xsl:apply-templates select="//metaGrammar/vLib[@id=$id]/*"/>
</xsl:template>

<xsl:template match="fs[@idref]">
  <xsl:variable name="id" select="@idref"/>
  <xsl:apply-templates select="//metaGrammar/vLib[@id=$id]/*"/>
</xsl:template>

<xsl:template match="var[@name and not(*)]">
  <xsl:text>var(</xsl:text>
  <xsl:apply-templates select="@name"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="var[@name and *]">
  <xsl:text>var(</xsl:text>
  <xsl:apply-templates select="@name"/>
  <xsl:text>)^</xsl:text>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="vAlt">
  <xsl:text>disj([</xsl:text>
  <xsl:apply-templates select="." mode="list"/>
  <xsl:text>])</xsl:text>
</xsl:template>

<xsl:template match="not/vAlt">
  <xsl:text>notdisj([</xsl:text>
  <xsl:apply-templates select="." mode="list"/>
  <xsl:text>])</xsl:text>
</xsl:template>

<xsl:template match="not[not(vAlt)]">
  <xsl:text>notdisj([</xsl:text>
  <xsl:apply-templates select="." mode="list"/>
  <xsl:text>])</xsl:text>
</xsl:template>

<xsl:template match="*" mode="list">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="not(position()=last())">, </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="*" mode="listnl">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="not(position()=last())">
      <xsl:text>, 
      </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="atomic">
  <xsl:param name="class"/>
  <xsl:text>reducer(</xsl:text>
  <xsl:value-of select="$class"/>
  <xsl:text>,
      </xsl:text>
  <xsl:apply-templates select="*" mode="equation"/>
  <xsl:text>
).

</xsl:text>
</xsl:template>

<xsl:template match="class/guard">
  <xsl:param name="class"/>
  <xsl:text>guard(</xsl:text>
  <xsl:value-of select="$class"/>
  <xsl:text>,
      </xsl:text>
  <xsl:apply-templates select="@name"/>
  <xsl:text>,</xsl:text>
  <xsl:value-of select="@rel"/>
  <xsl:text>,[
      </xsl:text>
      <xsl:apply-templates select="." mode="listnl"/>
      <xsl:text> 
      ]
).

</xsl:text>
</xsl:template>


<xsl:template match="guard">
  <xsl:text>[
      </xsl:text>
      <xsl:apply-templates select="." mode="listnl"/>
      <xsl:text> 
      ]
  </xsl:text>
</xsl:template>


<xsl:template match="guard/and">
  <xsl:text>and([</xsl:text>
  <xsl:apply-templates select="." mode="listnl"/>
  <xsl:text>])</xsl:text>
</xsl:template>

<xsl:template match="guard/equation|and/equation">
  <xsl:apply-templates select="*[1]" mode="equation"/>
  <xsl:choose>
    <xsl:when test="@default='+'">
      <xsl:text> == </xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text> = </xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:apply-templates select="*[2]" mode="equation"/>
</xsl:template>

<xsl:template match="class/equation">
  <xsl:param name="class"/>
  <xsl:text>equation(</xsl:text>
  <xsl:value-of select="$class"/>
  <xsl:text>,
     </xsl:text>
  <xsl:apply-templates select="*[1]" mode="equation"/>
  <xsl:text>,
     </xsl:text>
  <xsl:apply-templates select="*[2]" mode="equation"/>
  <xsl:text>
).

</xsl:text>
</xsl:template>

<xsl:template match="node|var|father" mode="equation">
  <xsl:value-of select="name()"/>
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="@name"/>
  <xsl:text>)</xsl:text>
  <xsl:apply-templates mode="path"/>
</xsl:template>

<xsl:template match="description" mode="equation">
  <xsl:text>desc </xsl:text>
  <xsl:apply-templates mode="path"/>
</xsl:template>

<xsl:template match="value" mode="equation">
  <xsl:text>value(</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="domain" mode="equation">
  <xsl:text>domain(</xsl:text>
  <xsl:apply-templates select="@value"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="property" mode="equation">
  <xsl:text>property(</xsl:text>
  <xsl:apply-templates select="@value"/>
  <xsl:text>)</xsl:text>
</xsl:template>


<xsl:template match="f" mode="path">
  <xsl:text> : </xsl:text>
  <xsl:apply-templates select="@name" mode="plquote"/>
  <xsl:apply-templates mode="path"/>
</xsl:template>

<xsl:template match="path" mode="path">
  <xsl:variable name="id" select="@idref"/>
  <xsl:apply-templates select="//metaGrammar/path[@id=$id]/*" mode="path"/>
  <xsl:apply-templates select="*" mode="path"/>
</xsl:template>

<xsl:template match="@name|@arg1|@arg2|@value">
  <xsl:call-template name="label">
    <xsl:with-param name="token" select="."/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="label">
  <xsl:param name="token"/>
  <xsl:choose>
    <xsl:when test="number($token)">
      <xsl:value-of select="$token"/>
    </xsl:when>
    <xsl:when test="contains($token,':')">
      <xsl:call-template name="plquote">
	<xsl:with-param name="token" select="substring-before($token,':')"/>
      </xsl:call-template>
      <xsl:text>!</xsl:text>
      <xsl:call-template name="label">
	<xsl:with-param name="token" select="substring-after($token,':')"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="plquote">
        <xsl:with-param name="token" select="$token"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
