<xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
        method="text"
        indent="no"
        encoding="ISO-8859-1"/>

<xsl:strip-space elements="*"/>

<xsl:template match="metaGrammar">
 <xsl:text>
digraph G {
  graph [ranksep = 1.2,   ratio = fill ];
  </xsl:text>
  <xsl:apply-templates select="class"/>
  <xsl:text>
}
  </xsl:text>
</xsl:template>

<xsl:template match="class">
    "<xsl:value-of select="@name"/>"
[ URL="#<xsl:value-of select="@name"/>" ];
    <xsl:apply-templates select="super"/>
</xsl:template>

<xsl:template match="super">
  "<xsl:value-of select="@name"/>"
    -&gt;
  "<xsl:value-of select="../@name"/>";
</xsl:template>

</xsl:stylesheet>
