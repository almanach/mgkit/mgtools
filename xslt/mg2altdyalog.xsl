<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:exslt="http://exslt.org/common"
  xmlns:func="http://exslt.org/func"
>

<xsl:output
        method="text"
        indent="no"
        encoding="ISO-8859-1"/>

<xsl:param name="tpl"/>

<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>

<xsl:variable name="plquote" select="concat($uppercase,'-$%:')"/>

<!-- xsl:key name="tpl" match="document('frenchmg.tpl.xml')/templates/template" use="@name" -->

<xsl:strip-space elements="*"/>

<xsl:template match="@*" mode="plquote">
  <xsl:choose>
    <xsl:when test="number(.)"><xsl:value-of select="."/>
    </xsl:when>
    <xsl:when test=". != translate(.,$plquote,'x')">
      <xsl:text>'</xsl:text><xsl:value-of select="."/><xsl:text>'</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="."/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="metaGrammar">
<xsl:text>

%% Header

:-op( 700, xfy, [&gt;&gt;,&gt;&gt;+]).
:-op( 700, fx, [&lt;]).
:-op( 700, fx, [node]).
:-op( 750, fx, [opt]).

%% ------

</xsl:text>
<xsl:apply-templates/>
</xsl:template>

<xsl:template match="class">
  <xsl:apply-templates select="@name" mode="plquote"/>
  <xsl:text> --> 
</xsl:text>
 <xsl:for-each select="*">
   <xsl:text>     </xsl:text>
   <xsl:apply-templates select="."/>,
 </xsl:for-each>
<xsl:text>     true
     .

</xsl:text>
</xsl:template>

<xsl:template match="super">
  <xsl:text>&lt; </xsl:text> <xsl:apply-templates select="@name" mode="plquote"/>
</xsl:template>

<xsl:template match="provides[@name]">
  <xsl:text>+ </xsl:text> <xsl:apply-templates select="@name" mode="plquote"/>
</xsl:template>

<xsl:template match="needs[@name]">
  <xsl:text>- </xsl:text> <xsl:apply-templates select="@name" mode="plquote"/>
</xsl:template>

<xsl:template match="description">
  <xsl:text>desc(</xsl:text><xsl:apply-templates/><xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="node">
  <xsl:if test="fs/f[@name = 'optional']">
    <xsl:text>opt </xsl:text>
  </xsl:if>
  <xsl:if test="fs/f[@name='cat']">
    <xsl:text>node </xsl:text>
  </xsl:if>
  <xsl:apply-templates select="@name" mode="plquote"/>
  <xsl:if test="fs">
    <xsl:text> : </xsl:text>
    <xsl:apply-templates select="fs"/>
  </xsl:if>
</xsl:template>

<xsl:template match="relation">
  <xsl:apply-templates select="@arg1" mode="plquote"/>
  <xsl:choose>
    <xsl:when test="@rel = 'father'">
      <xsl:text> &gt;&gt; </xsl:text>
    </xsl:when>
    <xsl:when test="@rel = 'dominates'">
      <xsl:text> &gt;&gt;+ </xsl:text>
    </xsl:when>
    <xsl:when test="@rel = 'precedes'">
      <xsl:text> &lt; </xsl:text>
    </xsl:when>
    <xsl:when test="@rel = 'equals'">
      <xsl:text> = </xsl:text>
    </xsl:when>
  </xsl:choose>
  <xsl:apply-templates select="@arg2" mode="plquote"/>
</xsl:template>

<xsl:template match="sym[@value]">
  <xsl:apply-templates select="@value" mode="plquote"/>
</xsl:template>

<xsl:template match="fs">
  <xsl:text>[</xsl:text>
  <xsl:for-each select="f">
    <xsl:apply-templates select="."/>
    <xsl:if test="not(position()=last())">, </xsl:if>
  </xsl:for-each>
  <xsl:text>]</xsl:text>
</xsl:template>

<xsl:template match="plus">
  <xsl:text>+</xsl:text>
</xsl:template>

<xsl:template match="minus">
  <xsl:text>-</xsl:text>
</xsl:template>

<xsl:template match="f[*]">
  <xsl:apply-templates select="@name" mode="plquote"/> 
  <xsl:text>: </xsl:text>
  <xsl:apply-templates/>
</xsl:template>

<!-- warning: be careful of extra newlines because of rigid taggify.pl -->
<xsl:template match="f[not(*)]">
  <xsl:apply-templates select="@name" mode="plquote"/>
  <xsl:text> : _</xsl:text>
</xsl:template>

<xsl:template match="sym[starts-with(@value,'@')]"> 
  <xsl:variable name="name" select="@value"/>
  [ %% macro = <xsl:value-of select="$name"/>
<xsl:text>
</xsl:text>
  <xsl:for-each select="document($tpl)/templates/template[@name = $name]/*">
    <xsl:apply-templates select="."/>
    <xsl:if test="not(position()=last())">, </xsl:if>
  </xsl:for-each>
]
</xsl:template>

<xsl:template match="var[@name and not(*)]">var(<xsl:apply-templates select="@name" mode="plquote"/>)
</xsl:template>

<xsl:template match="var[@name and *]">var(<xsl:apply-templates select="@name" mode="plquote"/>)^<xsl:apply-templates/>
</xsl:template>

<xsl:template match="vAlt">disj([<xsl:apply-templates select="." mode="list"/>])
</xsl:template>

<xsl:template match="not/vAlt">notdisj([<xsl:apply-templates select="." mode="list"/>])
</xsl:template>

<xsl:template match="not[not(vAlt)]">notdisj([<xsl:apply-templates select="." mode="list"/>])
</xsl:template>

<xsl:template match="*" mode="list">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="not(position()=last())">, </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="equation">
  <xsl:apply-templates select="*[1]" mode="equation"/> 
  <xsl:text> = </xsl:text>
  <xsl:apply-templates select="*[2]" mode="equation"/>
</xsl:template>

<xsl:template match="equation/node|equation/var|equation/father" mode="equation">
  <xsl:value-of select="name()"/>
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="@name" mode="plquote"/>
  <xsl:text>)</xsl:text>
  <xsl:apply-templates mode="path"/>
</xsl:template>

<xsl:template match="equation/description" mode="equation">
  <xsl:text>desc </xsl:text>
  <xsl:apply-templates mode="path"/>
</xsl:template>

<xsl:template match="f" mode="path">
  <xsl:text> : </xsl:text>
  <xsl:apply-templates select="@name" mode="plquote"/> 
  <xsl:apply-templates mode="path"/>
</xsl:template>

<xsl:template match="equation/value" mode="equation">
  <xsl:text>value(</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>)</xsl:text>
</xsl:template>

</xsl:stylesheet>
