<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
        method="xml"
	indent="yes"
	encoding="ISO-8859-1"/>

<xsl:template match="/tag">
  <tag>
        <xsl:apply-templates/>
  </tag>
</xsl:template>

<xsl:template match="tree">
 <family name="{@name}">
  <tree name="{@name}">
    <ht>
      <xsl:apply-templates select="description/fs/f[@name='ht']/*"/>
    </ht>
    <xsl:apply-templates/>
  </tree>
 </family>
</xsl:template>

<xsl:template match="*|@*|comment()|text()">
  <xsl:copy><xsl:apply-templates select="*|@*|comment()|text()"/></xsl:copy>
</xsl:template>

</xsl:stylesheet>

