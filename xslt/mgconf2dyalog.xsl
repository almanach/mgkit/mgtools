<xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
        method="text"
        indent="no"
        encoding="ISO-8859-1"/>

<xsl:strip-space elements="*"/>

<xsl:template match="/metaGrammar">
%% Automatically generated: do not edit

  <xsl:apply-templates/>

</xsl:template>

<xsl:template match="class[@disabled='yes']">
disabled_class('<xsl:value-of select="@name"/>').
</xsl:template>

</xsl:stylesheet>
