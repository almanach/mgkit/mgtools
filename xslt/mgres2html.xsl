<?xml version="1.0"?>
<!-- Author: Eric de la Clergerie "Eric.De_La_Clergerie@inria.fr" -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>

<xsl:output
  method="html"
  indent="yes"
  encoding="ISO-8859-1"/>

<xsl:key name="res" match="needs|provides" use="@name"/>

<xsl:template match="/metaGrammar">
   <html>
    <head>
      <link rel="STYLESHEET" type="text/css" href="style.css" />
      <title>Resources</title>
    </head>
    <body>
      <xsl:for-each select="(class/provides|class/needs)[generate-id() = generate-id(key('res',@name) [1])]">
        <xsl:sort select="@name"/>
        <xsl:variable name="name" select="@name"/>
        <a name="{$name}"/>
        <h1 class="resource"><xsl:value-of select="$name"/></h1>

        <ul>
          <xsl:for-each select="key('res',$name)">
            <xsl:sort select="../@name"/>
            <li class="class">
              <a href="classes.html#{../@name}" target="classes">
                <xsl:value-of select="../@name"/>
              </a>
              (<xsl:value-of select="name(.)"/>)
            </li>
          </xsl:for-each>
        </ul>

      </xsl:for-each>
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>
