#!/usr/bin/env perl -w
use Test::More;
use File::Basename;
use strict;

# compute plan
my @data = <DATA>;
plan tests => scalar @data;

my $dir = dirname($0);
my @output = split /\n/, `xsltproc $dir/../xslt/mg2dyalog.xsl $dir/sample.xml`;

foreach my $expected_output (@data) {
    chomp $expected_output;
    is(shift @output, $expected_output, "$expected_output");
}


__DATA__
class(categories).

class(adj).

super(adj,categories).

equation(adj,
     desc  : ht : cat,
     value(adj)
).

node(adj,'Nr').

nodefeature(adj,
    'Nr',
    [cat: 'N',
      toto: ',...']
).

node(adj,adj).

nodefeature(adj,
    adj,
    [cat: adj,
      type: anchor]
).

needs(adj,noun!'shallow_auxiliary').

equals(adj,'Nf',noun!'Foot').

equals(adj,'Nr',noun!'Root').

father(adj,'Nr',adj).

needs(adj,adj!agreement).

equals(adj,adj,adj!'N').

class(det).

super(det,categories).

equation(det,
     desc  : ht : cat,
     value(det)
).

node(det,det).

nodefeature(det,
    det,
    [cat: det,
      type: anchor]
).

class(noun).

super(noun,categories).

description(noun,[ht: [cat: noun]]).

node(noun,'N2').

nodefeature(noun,
    'N2',
    [cat: 'N2']
).

class(pnoun).

super(pnoun,noun).

father(pnoun,'N2','Np').

node(pnoun,'Np').

nodefeature(pnoun,
    'Np',
    [cat: np,
      type: anchor]
).

needs(pnoun,noun!agreement).

equals(pnoun,'Np',noun!'N').

class('_cnoun').

super('_cnoun',noun).

father('_cnoun','N2','N').

father('_cnoun','N','Nc').

father('_cnoun','N2',det).

precedes('_cnoun',det,'N').

node('_cnoun','N2').

nodefeature('_cnoun',
    'N2',
    [type: std]
).

node('_cnoun','N').

nodefeature('_cnoun',
    'N',
    [cat: 'N']
).

node('_cnoun','Nc').

nodefeature('_cnoun',
    'Nc',
    [type: anchor]
).

node('_cnoun',det).

nodefeature('_cnoun',
    det,
    [cat: det,
      type: subst]
).

needs('_cnoun',nc!agreement).

equals('_cnoun',nc,nc!'N').

needs('_cnoun',n!agreement).

equals('_cnoun','N',n!'N').

needs('_cnoun',det!agreement).

equals('_cnoun',det,det!'N').

class(cnoun).

super(cnoun,'_cnoun').

node(cnoun,'Nc').

nodefeature(cnoun,
    'Nc',
    [cat: nc]
).

class('pnoun_as_cnoun').

super('pnoun_as_cnoun','_cnoun').

node('pnoun_as_cnoun','Nc').

nodefeature('pnoun_as_cnoun',
    'Nc',
    [cat: np]
).

class(verbs).

super(verbs,categories).

needs(verbs,'real_verb').

class(prep).

super(prep,categories).

equation(prep,
     desc  : ht : cat,
     value(prep)
).

equation(prep,
     desc  : ht : pcas,
     node(prep) : bot : pcas
).

equation(prep,
     desc  : ht : pp,
     node('Group') : cat
).

father(prep,'PP','Group').

father(prep,'PP',prep).

precedes(prep,prep,'Group').

node(prep,'PP').

nodefeature(prep,
    'PP',
    [cat: 'PP']
).

node(prep,'N2').

nodefeature(prep,
    'N2',
    [type: subst]
).

node(prep,prep).

nodefeature(prep,
    prep,
    [cat: prep,
      type: anchor]
).

class('prep_N2').

super('prep_N2',prep).

equals('prep_N2','N2','Group').

node('prep_N2','N2').

nodefeature('prep_N2',
    'N2',
    [cat: 'N2']
).

class('prep_S').

super('prep_S',prep).

equals('prep_S','S','Group').

node('prep_S','S').

nodefeature('prep_S',
    'S',
    [cat: 'S',
      top: [mode: infinitive]]
).

class(csu).

super(csu,categories).

equation(csu,
     desc  : ht : cat,
     value(csu)
).

precedes(csu,'C','S').

node(csu,'S').

nodefeature(csu,
    'S',
    [cat: 'S',
      type: subst]
).

node(csu,'WS').

nodefeature(csu,
    'WS',
    [cat: 'WS']
).

node(csu,'C').

nodefeature(csu,
    'C',
    [cat: csu,
      type: anchor]
).

father(csu,'WS','C').

father(csu,'WS','S').

equation(csu,
     node('WS') : bot : lfg,
     node('S') : top : lfg
).

class(adv).

super(adv,categories).

equation(adv,
     desc  : ht : cat,
     value(adv)
).

node(adv,'Adv').

nodefeature(adv,
    'Adv',
    [cat: adv,
      type: anchor]
).

class('adv_leaf').

super('adv_leaf',adv).

class('adv_modifier').

super('adv_modifier',adv).

needs('adv_modifier','H'!'shallow_auxiliary').

equals('adv_modifier','Hf','H'!'Foot').

equals('adv_modifier','Hr','H'!'Root').

father('adv_modifier','Hr','Adv').

class('adv_v').

super('adv_v','adv_modifier').

node('adv_v','Vr').

nodefeature('adv_v',
    'Vr',
    [cat: 'V']
).

equals('adv_v','Hr','Vr').

equals('adv_v','Hf','Vf').

class('adv_adv').

super('adv_adv','adv_modifier').

node('adv_adv','Advr').

nodefeature('adv_adv',
    'Advr',
    [cat: adv]
).

equals('adv_adv','Hr','Advr').

equals('adv_adv','Hf','Advf').

precedes('adv_adv','Adv','Advf').

class('adv_adj').

super('adv_adj','adv_modifier').

node('adv_adj','Adjr').

nodefeature('adv_adj',
    'Adjr',
    [cat: adj]
).

equals('adv_adj','Hr','Adjr').

equals('adv_adj','Hf','Adjf').

precedes('adv_adj','Adv','Adjf').

class('adv_s').

super('adv_s','adv_modifier').

node('adv_s','Sr').

nodefeature('adv_s',
    'Sr',
    [cat: 'S']
).

equals('adv_s','Hr','Sr').

equals('adv_s','Hf','Sf').

class(agreement).

provides(agreement,agreement).

equation(agreement,
     father('N') : bot : number,
     node('N') : top : number
).

equation(agreement,
     father('N') : bot : gender,
     node('N') : top : gender
).

class(auxiliary).

dominates(auxiliary,'Root','Foot').

equation(auxiliary,
     node('Root') : cat,
     node('Foot') : cat
).

equation(auxiliary,
     node('Foot') : type,
     value(foot)
).

class('deep_auxiliary').

provides('deep_auxiliary','deep_auxiliary').

class('shallow_auxiliary').

provides('shallow_auxiliary','shallow_auxiliary').

equation('shallow_auxiliary',
     node('Root') : bot,
     node('Foot') : top
).

father('shallow_auxiliary','Root','Foot').
