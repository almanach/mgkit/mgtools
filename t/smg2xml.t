#!/usr/bin/env perl -w
use Test::More;
use File::Basename;
use strict;

# compute plan
my @data = <DATA>;
plan tests => scalar @data;

my $dir = dirname($0);
my @output = split /\n/, `./smg2xml $dir/sample.smg | xmllint -format -`;

foreach my $expected_output (@data) {
    chomp $expected_output;
    is(shift @output, $expected_output, "$expected_output");
}


__DATA__
<?xml version="1.0" encoding="ISO-8859-1"?>
<metaGrammar>
  <class name="categories">
    <!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 All syntactic categories
-->
  </class>
  <class name="adj">
    <!-- saghgashgash 
-->
    <super name="categories"/>
    <equation>
      <description>
        <f name="ht">
          <f name="cat"/>
        </f>
      </description>
      <value>
        <symbol value="adj"/>
      </value>
    </equation>
    <node name="Nr" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="N"/>
        </f>
        <f name="toto">
          <symbol value=",..."/>
        </f>
      </fs>
    </node>
    <node name="adj" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="adj"/>
        </f>
        <f name="type">
          <symbol value="anchor"/>
        </f>
      </fs>
    </node>
    <needs name="noun:shallow_auxiliary"/>
    <relation rel="equals" arg1="Nf" arg2="noun:Foot"/>
    <relation rel="equals" arg1="Nr" arg2="noun:Root"/>
    <relation rel="father" arg1="Nr" arg2="adj"/>
    <needs name="adj:agreement"/>
    <relation rel="equals" arg1="adj" arg2="adj:N"/>
  </class>
  <class name="det">
    <super name="categories"/>
    <equation>
      <description>
        <f name="ht">
          <f name="cat"/>
        </f>
      </description>
      <value>
        <symbol value="det"/>
      </value>
    </equation>
    <node name="det" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="det"/>
        </f>
        <f name="type">
          <symbol value="anchor"/>
        </f>
      </fs>
    </node>
  </class>
  <class name="noun">
    <super name="categories"/>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="noun"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node name="N2" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="N2"/>
        </f>
      </fs>
    </node>
  </class>
  <class name="pnoun">
    <super name="noun"/>
    <relation rel="father" arg1="N2" arg2="Np"/>
    <node name="Np" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="np"/>
        </f>
        <f name="type">
          <symbol value="anchor"/>
        </f>
      </fs>
    </node>
    <needs name="noun:agreement"/>
    <relation rel="equals" arg1="Np" arg2="noun:N"/>
  </class>
  <class name="_cnoun">
    <super name="noun"/>
    <relation rel="father" arg1="N2" arg2="N"/>
    <relation rel="father" arg1="N" arg2="Nc"/>
    <relation rel="father" arg1="N2" arg2="det"/>
    <relation rel="precedes" arg1="det" arg2="N"/>
    <node name="N2" virtual="no">
      <fs>
        <f name="type">
          <symbol value="std"/>
        </f>
      </fs>
    </node>
    <node name="N" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="N"/>
        </f>
      </fs>
    </node>
    <node name="Nc" virtual="no">
      <fs>
        <f name="type">
          <symbol value="anchor"/>
        </f>
      </fs>
    </node>
    <node name="det" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="det"/>
        </f>
        <f name="type">
          <symbol value="subst"/>
        </f>
      </fs>
    </node>
    <needs name="nc:agreement"/>
    <relation rel="equals" arg1="nc" arg2="nc:N"/>
    <needs name="n:agreement"/>
    <relation rel="equals" arg1="N" arg2="n:N"/>
    <needs name="det:agreement"/>
    <relation rel="equals" arg1="det" arg2="det:N"/>
  </class>
  <class name="cnoun">
    <super name="_cnoun"/>
    <node name="Nc" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="nc"/>
        </f>
      </fs>
    </node>
  </class>
  <class name="pnoun_as_cnoun">
    <super name="_cnoun"/>
    <node name="Nc" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="np"/>
        </f>
      </fs>
    </node>
  </class>
  <class name="verbs">
    <super name="categories"/>
    <needs name="real_verb"/>
  </class>
  <class name="prep">
    <super name="categories"/>
    <equation>
      <description>
        <f name="ht">
          <f name="cat"/>
        </f>
      </description>
      <value>
        <symbol value="prep"/>
      </value>
    </equation>
    <equation>
      <description>
        <f name="ht">
          <f name="pcas"/>
        </f>
      </description>
      <node name="prep">
        <f name="bot">
          <f name="pcas"/>
        </f>
      </node>
    </equation>
    <equation>
      <description>
        <f name="ht">
          <f name="pp"/>
        </f>
      </description>
      <node name="Group">
        <f name="cat"/>
      </node>
    </equation>
    <relation rel="father" arg1="PP" arg2="Group"/>
    <relation rel="father" arg1="PP" arg2="prep"/>
    <relation rel="precedes" arg1="prep" arg2="Group"/>
    <node name="PP" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="PP"/>
        </f>
      </fs>
    </node>
    <node name="N2" virtual="no">
      <fs>
        <f name="type">
          <symbol value="subst"/>
        </f>
      </fs>
    </node>
    <node name="prep" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="prep"/>
        </f>
        <f name="type">
          <symbol value="anchor"/>
        </f>
      </fs>
    </node>
  </class>
  <class name="prep_N2">
    <super name="prep"/>
    <relation rel="equals" arg1="N2" arg2="Group"/>
    <node name="N2" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="N2"/>
        </f>
      </fs>
    </node>
  </class>
  <class name="prep_S">
    <super name="prep"/>
    <relation rel="equals" arg1="S" arg2="Group"/>
    <node name="S" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="S"/>
        </f>
        <f name="top">
          <fs>
            <f name="mode">
              <symbol value="infinitive"/>
            </f>
          </fs>
        </f>
      </fs>
    </node>
  </class>
  <class name="csu">
    <super name="categories"/>
    <equation>
      <description>
        <f name="ht">
          <f name="cat"/>
        </f>
      </description>
      <value>
        <symbol value="csu"/>
      </value>
    </equation>
    <relation rel="precedes" arg1="C" arg2="S"/>
    <node name="S" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="S"/>
        </f>
        <f name="type">
          <symbol value="subst"/>
        </f>
      </fs>
    </node>
    <node name="WS" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="WS"/>
        </f>
      </fs>
    </node>
    <node name="C" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="csu"/>
        </f>
        <f name="type">
          <symbol value="anchor"/>
        </f>
      </fs>
    </node>
    <relation rel="father" arg1="WS" arg2="C"/>
    <relation rel="father" arg1="WS" arg2="S"/>
    <equation>
      <node name="WS">
        <f name="bot">
          <f name="lfg"/>
        </f>
      </node>
      <node name="S">
        <f name="top">
          <f name="lfg"/>
        </f>
      </node>
    </equation>
  </class>
  <class name="adv">
    <super name="categories"/>
    <equation>
      <description>
        <f name="ht">
          <f name="cat"/>
        </f>
      </description>
      <value>
        <symbol value="adv"/>
      </value>
    </equation>
    <node name="Adv" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="adv"/>
        </f>
        <f name="type">
          <symbol value="anchor"/>
        </f>
      </fs>
    </node>
  </class>
  <class name="adv_leaf">
    <super name="adv"/>
  </class>
  <class name="adv_modifier">
    <super name="adv"/>
    <needs name="H:shallow_auxiliary"/>
    <relation rel="equals" arg1="Hf" arg2="H:Foot"/>
    <relation rel="equals" arg1="Hr" arg2="H:Root"/>
    <relation rel="father" arg1="Hr" arg2="Adv"/>
  </class>
  <class name="adv_v">
    <super name="adv_modifier"/>
    <node name="Vr" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="V"/>
        </f>
      </fs>
    </node>
    <relation rel="equals" arg1="Hr" arg2="Vr"/>
    <relation rel="equals" arg1="Hf" arg2="Vf"/>
  </class>
  <class name="adv_adv">
    <super name="adv_modifier"/>
    <node name="Advr" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="adv"/>
        </f>
      </fs>
    </node>
    <relation rel="equals" arg1="Hr" arg2="Advr"/>
    <relation rel="equals" arg1="Hf" arg2="Advf"/>
    <relation rel="precedes" arg1="Adv" arg2="Advf"/>
  </class>
  <class name="adv_adj">
    <super name="adv_modifier"/>
    <node name="Adjr" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="adj"/>
        </f>
      </fs>
    </node>
    <relation rel="equals" arg1="Hr" arg2="Adjr"/>
    <relation rel="equals" arg1="Hf" arg2="Adjf"/>
    <relation rel="precedes" arg1="Adv" arg2="Adjf"/>
  </class>
  <class name="adv_s">
    <super name="adv_modifier"/>
    <node name="Sr" virtual="no">
      <fs>
        <f name="cat">
          <symbol value="S"/>
        </f>
      </fs>
    </node>
    <relation rel="equals" arg1="Hr" arg2="Sr"/>
    <relation rel="equals" arg1="Hf" arg2="Sf"/>
  </class>
  <class name="agreement">
    <provides name="agreement"/>
    <equation>
      <father name="N">
        <f name="bot">
          <f name="number"/>
        </f>
      </father>
      <node name="N">
        <f name="top">
          <f name="number"/>
        </f>
      </node>
    </equation>
    <equation>
      <father name="N">
        <f name="bot">
          <f name="gender"/>
        </f>
      </father>
      <node name="N">
        <f name="top">
          <f name="gender"/>
        </f>
      </node>
    </equation>
  </class>
  <class name="auxiliary">
    <relation rel="dominates" arg1="Root" arg2="Foot"/>
    <equation>
      <node name="Root">
        <f name="cat"/>
      </node>
      <node name="Foot">
        <f name="cat"/>
      </node>
    </equation>
    <equation>
      <node name="Foot">
        <f name="type"/>
      </node>
      <value>
        <symbol value="foot"/>
      </value>
    </equation>
  </class>
  <class name="deep_auxiliary">
    <provides name="deep_auxiliary"/>
  </class>
  <class name="shallow_auxiliary">
    <provides name="shallow_auxiliary"/>
    <equation>
      <node name="Root">
        <f name="bot"/>
      </node>
      <node name="Foot">
        <f name="top"/>
      </node>
    </equation>
    <relation rel="father" arg1="Root" arg2="Foot"/>
  </class>
</metaGrammar>
