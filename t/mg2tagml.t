#!/usr/bin/env perl -w
use Test::More;
use File::Basename;
use strict;

# compute plan
my @data = <DATA>;
plan tests => scalar @data;

my $dir = dirname($0);

my @output = split /\n/, `xsltproc $dir/../xslt/mg2tagml.xsl $dir/sample.mg`;
foreach my $expected_output (@data) {
    chomp $expected_output;
    is(shift @output, $expected_output, "$expected_output");
}


__DATA__
<?xml version="1.0" encoding="ISO-8859-1"?>
<tag>
  <family name="0 det"><tree name="0 det"><ht><fs>
            <f name="cat">
              <symbol value="det"/>
            </f>
          </fs></ht>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="det"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="det" cat="det" type="anchor"/>
  </tree></family>
  <family name="1 prep_N2"><tree name="1 prep_N2"><ht><fs>
            <f name="cat">
              <symbol value="prep"/>
            </f>
            <f name="pcas">
              <var name="pcas@4"/>
            </f>
            <f name="pp">
              <symbol value="N2"/>
            </f>
          </fs></ht>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="prep"/>
            </f>
            <f name="pcas">
              <var name="pcas@4"/>
            </f>
            <f name="pp">
              <symbol value="N2"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="PP" cat="PP" type="std">
      <node adj="yes" id="prep" cat="prep" type="anchor">
        <narg type="bot">
          <fs>
            <f name="pcas">
              <var name="pcas@4"/>
            </f>
          </fs>
        </narg>
      </node>
      <node adj="no" id="N2" cat="N2" type="subst"/>
    </node>
  </tree></family>
  <family name="2 prep_S"><tree name="2 prep_S"><ht><fs>
            <f name="cat">
              <symbol value="prep"/>
            </f>
            <f name="pcas">
              <var name="pcas@4"/>
            </f>
            <f name="pp">
              <symbol value="S"/>
            </f>
          </fs></ht>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="prep"/>
            </f>
            <f name="pcas">
              <var name="pcas@4"/>
            </f>
            <f name="pp">
              <symbol value="S"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="PP" cat="PP" type="std">
      <node adj="yes" id="prep" cat="prep" type="anchor">
        <narg type="bot">
          <fs>
            <f name="pcas">
              <var name="pcas@4"/>
            </f>
          </fs>
        </narg>
      </node>
      <node adj="yes" id="S" cat="S" type="std">
        <narg type="top">
          <fs>
            <f name="mode">
              <symbol value="infinitive"/>
            </f>
          </fs>
        </narg>
      </node>
    </node>
  </tree></family>
  <family name="3 prep_S"><tree name="3 prep_S"><ht><fs>
            <f name="cat">
              <symbol value="prep"/>
            </f>
            <f name="pcas">
              <var name="pcas@4"/>
            </f>
            <f name="pp">
              <symbol value="S"/>
            </f>
          </fs></ht>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="prep"/>
            </f>
            <f name="pcas">
              <var name="pcas@4"/>
            </f>
            <f name="pp">
              <symbol value="S"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="no" id="N2" type="subst"/>
  </tree></family>
  <family name="4 csu"><tree name="4 csu"><ht><fs>
            <f name="cat">
              <symbol value="csu"/>
            </f>
          </fs></ht>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="csu"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="WS" cat="WS" type="std">
      <narg type="bot">
        <fs>
          <f name="lfg">
            <var name="lfg@7"/>
          </f>
        </fs>
      </narg>
      <node adj="yes" id="C" cat="csu" type="anchor"/>
      <node adj="no" id="S" cat="S" type="subst">
        <narg type="top">
          <fs>
            <f name="lfg">
              <var name="lfg@7"/>
            </f>
          </fs>
        </narg>
      </node>
    </node>
  </tree></family>
  <family name="5 adv_leaf"><tree name="5 adv_leaf"><ht><fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs></ht>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="Adv" cat="adv" type="anchor"/>
  </tree></family>
  <family name="6 adv_s H:shallow_auxiliary"><tree name="6 adv_s H:shallow_auxiliary"><ht><fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs></ht>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="Sr" cat="S" type="std">
      <narg type="bot">
        <var name="H:top@14"/>
      </narg>
      <node adj="yes" id="Adv" cat="adv" type="anchor"/>
    </node>
  </tree></family>
  <family name="7 adv_adj H:shallow_auxiliary"><tree name="7 adv_adj H:shallow_auxiliary"><ht><fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs></ht>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="Adjr" cat="adj" type="std">
      <narg type="bot">
        <var name="H:top@14"/>
      </narg>
      <node adj="yes" id="Adv" cat="adv" type="anchor"/>
    </node>
  </tree></family>
  <family name="8 adv_adv H:shallow_auxiliary"><tree name="8 adv_adv H:shallow_auxiliary"><ht><fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs></ht>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="Advr" cat="adv" type="std">
      <narg type="bot">
        <var name="H:top@14"/>
      </narg>
      <node adj="yes" id="Adv" cat="adv" type="anchor"/>
    </node>
  </tree></family>
  <family name="9 adv_v H:shallow_auxiliary"><tree name="9 adv_v H:shallow_auxiliary"><ht><fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs></ht>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="adv"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="Vr" cat="V" type="std">
      <narg type="bot">
        <var name="H:top@14"/>
      </narg>
      <node adj="yes" id="Adv" cat="adv" type="anchor"/>
    </node>
  </tree></family>
  <family name="10 noun:agreement pnoun"><tree name="10 noun:agreement pnoun"><ht><fs>
            <f name="cat">
              <symbol value="noun"/>
            </f>
          </fs></ht>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="noun"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="N2" cat="N2" type="std">
      <narg type="bot">
        <fs>
          <f name="gender">
            <var name="noun:gender@16"/>
          </f>
          <f name="number">
            <var name="noun:number@18"/>
          </f>
        </fs>
      </narg>
      <node adj="yes" id="Np" cat="np" type="anchor">
        <narg type="top">
          <fs>
            <f name="gender">
              <var name="noun:gender@16"/>
            </f>
            <f name="number">
              <var name="noun:number@18"/>
            </f>
          </fs>
        </narg>
      </node>
    </node>
  </tree></family>
  <family name="11 adj adj:agreement noun:shallow_auxiliary"><tree name="11 adj adj:agreement noun:shallow_auxiliary"><ht><fs>
            <f name="cat">
              <symbol value="adj"/>
            </f>
          </fs></ht>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="adj"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="Nr" cat="N" type="std">
      <narg type="bot">
        <var name="noun:top@14">
          <fs>
            <f name="gender">
              <var name="adj:gender@16"/>
            </f>
            <f name="number">
              <var name="adj:number@18"/>
            </f>
          </fs>
        </var>
      </narg>
      <node adj="yes" id="adj" cat="adj" type="anchor">
        <narg type="top">
          <fs>
            <f name="gender">
              <var name="adj:gender@16"/>
            </f>
            <f name="number">
              <var name="adj:number@18"/>
            </f>
          </fs>
        </narg>
      </node>
    </node>
  </tree></family>
  <family name="12 det:agreement n:agreement nc:agreement pnoun_as_cnoun"><tree name="12 det:agreement n:agreement nc:agreement pnoun_as_cnoun"><ht><fs>
            <f name="cat">
              <symbol value="noun"/>
            </f>
          </fs></ht>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="noun"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="N2" cat="N2" type="std">
      <narg type="bot">
        <fs>
          <f name="gender">
            <var name="n:gender@16"/>
          </f>
          <f name="number">
            <var name="n:number@18"/>
          </f>
        </fs>
      </narg>
      <node adj="no" id="det" cat="det" type="subst">
        <narg type="top">
          <fs>
            <f name="gender">
              <var name="n:gender@16"/>
            </f>
            <f name="number">
              <var name="n:number@18"/>
            </f>
          </fs>
        </narg>
      </node>
      <node adj="yes" id="N" cat="N" type="std">
        <narg type="top">
          <fs>
            <f name="gender">
              <var name="n:gender@16"/>
            </f>
            <f name="number">
              <var name="n:number@18"/>
            </f>
          </fs>
        </narg>
        <node adj="yes" id="Nc" cat="np" type="anchor"/>
      </node>
    </node>
  </tree></family>
  <family name="13 det:agreement n:agreement nc:agreement cnoun"><tree name="13 det:agreement n:agreement nc:agreement cnoun"><ht><fs>
            <f name="cat">
              <symbol value="noun"/>
            </f>
          </fs></ht>
    <description>
      <fs>
        <f name="ht">
          <fs>
            <f name="cat">
              <symbol value="noun"/>
            </f>
          </fs>
        </f>
      </fs>
    </description>
    <node adj="yes" id="N2" cat="N2" type="std">
      <narg type="bot">
        <fs>
          <f name="gender">
            <var name="n:gender@16"/>
          </f>
          <f name="number">
            <var name="n:number@18"/>
          </f>
        </fs>
      </narg>
      <node adj="no" id="det" cat="det" type="subst">
        <narg type="top">
          <fs>
            <f name="gender">
              <var name="n:gender@16"/>
            </f>
            <f name="number">
              <var name="n:number@18"/>
            </f>
          </fs>
        </narg>
      </node>
      <node adj="yes" id="N" cat="N" type="std">
        <narg type="top">
          <fs>
            <f name="gender">
              <var name="n:gender@16"/>
            </f>
            <f name="number">
              <var name="n:number@18"/>
            </f>
          </fs>
        </narg>
        <node adj="yes" id="Nc" cat="nc" type="anchor"/>
      </node>
    </node>
  </tree></family>
</tag>
