/* Hash code: taken from Andrew Appel's book */
#define NULL 0
#include <stdlib.h>
#include <stdio.h>

struct bucket {
  char* key;
  void* binding;
  struct bucket *next;
};

#define SIZE 109

struct bucket *table[SIZE];

unsigned int hash (char *s0) {
  unsigned int h=0 ;
  char *s ;
  for (s=s0; *s; s++)
    h = h*65599 + *s ;
  return h ;
}

struct bucket *Bucket (char* key, void *binding, struct bucket *next) {
  struct bucket *b = (struct bucket *) malloc (sizeof (*b)) ;
  if (!b) {
    printf ("Error: cannot malloc in Bucket (symbol table management)\nkey=%s\n", key) ;
    exit (1) ;
  }
  b->key = key ;
  b->binding = binding ;
  b->next = next ;
  return b;
}

void insert (char* key, void *binding) {
  int index = hash (key) % SIZE ;
  table[index] = Bucket ( key, binding, table [index] ) ;
}

void *lookup (char* key) {
  int index = hash (key) % SIZE ;
  struct bucket *b ;
  for ( b=table[index]; b; b=b->next ) {
    if (0 == strcmp (b->key,key)) return b->binding ;
  }
  return NULL ;
}

void *pop (char* key) {
  int index = hash (key) % SIZE ;
  table[index] = table[index]->next ;
}

void iter_tab (void f (char*,void*)) {
  int index ;
  struct bucket *b ;
  for (index=0; index<SIZE; index++) {
    b =table[index] ;
    if (b) {
      for ( b; b; b=b->next ) {
	f (b->key,b->binding) ;
      }
    }
  }
}
