%{ 

#include <stdlib.h>

#define YYSTYPE char*
#include "parser.h"
extern YYSTYPE yylval;
//#define DEBUGLEX 0
#ifdef DEBUGLEX
#define DBUGLEX(x) x
#else
#define DBUGLEX(x)
#endif

char *xmlstrdup(char *);

void yyerror(char *s) {
  printf ("syntax error:\t%s\n\t\tat line number %d\n",
	  s, yylineno);
  exit(1);
}

#ifndef HAVE_STRNDUP

size_t
my_strnlen (const char *string, size_t maxlen)
{
  const char *end = memchr (string, '\0', maxlen);
  return end ? (size_t) (end - string) : maxlen;
}

char *
strndup (s, n)
     const char *s;
     size_t n;
{
  size_t len = my_strnlen (s, n);
  char *new = malloc (len + 1);

  if (new == NULL)
    return NULL;

  new[len] = '\0';
  return (char *) memcpy (new, s, len);
}
#endif

%}

LOWACC       [�������������������] 
UPACC        [�������������������]
ALPHA        [[:alnum:]_-]|{LOWACC}|{UPACC}

LAYOUT       [ \t\r\n]
%option stack yylineno 8bit

%%
{LAYOUT}+   ;
("\\\\"[^\n]*"\n") {
                 yylval = (char*) xmlstrdup (yytext+2);
                 return DOCUMENTATION;
                 } ;
("%"[^\n]*"\n") ;
"("    {DBUGLEX(printf ("LPAREN\n");) ; return LPAREN ;};
")"    {DBUGLEX(printf ("RPAREN\n");) ; return RPAREN ;};
"{"    {DBUGLEX(printf ("LCURLYBK\n");) ; return LCURLYBK ;};
"}"    {DBUGLEX(printf ("RCURLYBK\n");) ; return RCURLYBK ;};
"["    {DBUGLEX(printf ("LSQBK\n");) ; return LSQBK ;};
"]"    {DBUGLEX(printf ("RSQBK\n");) ; return RSQBK ;};
","    {DBUGLEX(printf ("COMMA\n");) ; return COMMA ;};
"<:"   {DBUGLEX(printf ("INHERIT\n");) ; return INHERIT ;};
"<"    {DBUGLEX(printf ("PREC\n");) ; return PREC ;};
">>"   {DBUGLEX(printf (">>\n");) ; return FATHERREL ;};
">>+"  {DBUGLEX(printf ("ANCESTOR\n");) ; return ANCESTOR ;};
"-"    {DBUGLEX(printf ("MINUS\n");) ; return MINUS ;};
"+"    {DBUGLEX(printf ("PLUS\n");) ; return PLUS ;};
"++"    {DBUGLEX(printf ("XPLUS\n");) ; return XPLUS ;};
"*"    {DBUGLEX(printf ("STAR\n");) ; return STAR ;};
"~"    {DBUGLEX(printf ("NEG\n");) ; return NEG ;};
":"    {DBUGLEX(printf ("COLON\n");) ; return COLON ;};
"::"   {DBUGLEX(printf ("DOUBLECOLON\n");) ; return DOUBLECOLON ;};
";"    {DBUGLEX(printf ("SEMICOLON\n");) ; return SEMICOLON ;};
"="    {DBUGLEX(printf ("=\n");) ; return EQUAL ;};
"=="    {DBUGLEX(printf ("=\n");) ; return XEQUAL ;};
"."    {DBUGLEX(printf ("DOT\n");) ; return DOT ;};
"|"    {DBUGLEX(printf ("ALT\n");) ; return ALT ;};
"!"    {DBUGLEX(printf ("BANG\n");) ; return BANG ;};
"?"    {DBUGLEX(printf ("ATMOST\n");) ; return ATMOST ;};
"=>"    {DBUGLEX(printf ("IMP\n");) ; return IMP ;};
"^"    {DBUGLEX(printf ("JOIN\n");) ; return JOIN ;};
"desc" {DBUGLEX(printf ("DESC\n");) ; return DESC ;};
"node" {DBUGLEX(printf ("NODE\n");) ; return NODE ;};
"opt"  {DBUGLEX(printf ("NODE\n");) ; return OPT ;};
"father"   {DBUGLEX(printf ("FATHER\n");) ; return FATHER ;};
"value"    {DBUGLEX(printf ("VALUE\n");) ; return VALUE ;};
"domain"    {DBUGLEX(printf ("DOMAIN\n");) ; return DOMAIN ;};
"property"    {DBUGLEX(printf ("PROPERTY\n");) ; return PROPERTY ;};
"$"      {DBUGLEX(printf ("VAR\n");) ; return VAR ;};
"class"    {DBUGLEX(printf ("\nCLASS\n");) ; return CLASS ;};
"template" {DBUGLEX(printf ("TEMPLATE\n");) ; return TEMPLATE ;};
"path"     {DBUGLEX(printf ("PATH\n");) ; return PATH ;};
"disable"  {DBUGLEX(printf ("DISABLE\n");) ; return DISABLE ;};
{ALPHA}+ {
                      yylval=(char*)xmlstrdup (yytext);
		      DBUGLEX(printf ("IDENT='%s' (%d)\n", yylval, yylineno));
                      return IDENT ;
                      } ;

"'"[^']*"'" {
                      yylval = (char *)strndup (yytext+1,strlen(yytext)-2);
                      yylval=(char*)xmlstrdup (yylval);
		      DBUGLEX(printf ("QUOTED IDENT='%s' (%d)\n", yylval, yylineno));
                      return IDENT ;
             }

\"[^\"]*\" {
                      yylval = (char *)strndup (yytext+1,strlen(yytext)-2);
                      yylval=(char*)xmlstrdup (yylval);
		      DBUGLEX(printf ("QUOTED IDENT='%s' (%d)\n", yylval, yylineno));
                      return IDENT ;
             }

@{ALPHA}+ {
                      yylval=(char*)xmlstrdup (yytext);
		      DBUGLEX(printf ("MACRONAME='%s' (%d)\n", yylval, yylineno));
                      return MACRONAME ;
                      } ;
"/*" {
       register int c ;
       for (;;) {
	 while ( (c=input()) != '*' && c != EOF ) ;
	 if  (c=='*') {
	   while  ( (c=input()) == '*' ) ;
	   if ( c == '/' ) break ;
	 }
       }
} ;
%%
int yywrap ()
{
  return 1;
}
