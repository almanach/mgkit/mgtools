void insert (char* key, void *binding) ;
void *lookup (char* key) ;
void *pop (char* key) ;
void iter_tab (void f (char*,void*)) ;
